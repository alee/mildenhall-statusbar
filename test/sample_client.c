/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <gio/gio.h>
#include <mildenhall/mildenhall.h>

static MildenhallStatusbar *proxy = NULL;
static void launcher_proxy_clb( GObject *source_object, GAsyncResult *res,gpointer user_data);
#if 0
static void set_status_bar_clb( GObject *source_object,GAsyncResult *res,gpointer user_data)
{

	GError *error = NULL;
	mildenhall_statusbar_call_set_status_bar_info_finish(proxy,res,&error);
}
#endif
static void remove_status_bar_cb( GObject *source_object,GAsyncResult *res,gpointer user_data)
{
	GError *error = NULL;
	gboolean out_status;
	mildenhall_statusbar_call_remove_status_bar_info_finish (proxy,&out_status,res,&error);
}



static void
name_appeared (GDBusConnection *connection,
		const gchar     *name,
		const gchar     *name_owner,
		gpointer         user_data)
{
	g_print("name_appeared \n");

	/* Asynchronously creates a proxy for the D-Bus interface */
	mildenhall_statusbar_proxy_new  (
			connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Mildenhall.StatusBar",
			"/org/apertis/Mildenhall/StatusBar",
			NULL,
			launcher_proxy_clb,
			NULL);
}

static void launcher_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer user_data)
{
	GError *error;
	g_print("Launcher Proxy \n");
	/* finishes the proxy creation and gets the proxy ptr */
	proxy = mildenhall_statusbar_proxy_new_finish(res , &error);

	if(proxy == NULL)
	{
		g_print("error %s \n",g_dbus_error_get_remote_error(error));
	}
	//App_Name,Status,Texture,Text
//	mildenhall_statusbar_call_set_status_bar_info (proxy,"Second","/home/mildenhall/mildenhallph2/data/mmd-apps/icons/icon_dvd-player.png","Second",NULL,set_status_bar_clb,NULL);
	mildenhall_statusbar_call_remove_status_bar_info (proxy,"First",NULL,remove_status_bar_cb,NULL);


	// ret = e_core_sample_service_call_populate_items_sync(proxy ,"Launcher", &a,&b,&c,&d,NULL, &error);


}

static void
name_vanished(GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data)
{
	g_print("name_vanished \n");
	if(NULL != proxy)
		g_object_unref(proxy);
}

gint main(gint argc, gchar *argv[])
{
	GMainLoop *mainloop = NULL;
	//  g_type_init();

	// if (! g_thread_supported ())
	//   g_thread_init (NULL);



	/* Starts watching name on the bus specified by bus_type */
	/* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */
	g_bus_watch_name ( G_BUS_TYPE_SESSION,
			"org.apertis.Mildenhall.StatusBar",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			name_appeared,
			name_vanished,
			NULL,
			NULL);

	mainloop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (mainloop);

	exit (0);
}
