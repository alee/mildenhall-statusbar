/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* status_bar.c
 *
 * Created on: Dec 18,2012
 * Author: Thushara.M.S
 *
 * status_bar.c */

#include "status_bar.h"

G_DEFINE_TYPE (StatusBar, status_bar, CLUTTER_TYPE_ACTOR)

#define BAR_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), STATUS_TYPE_BAR, StatusBarPrivate))

#define DEFAULT_TEXT				"default-text"
#define	DEFAULT_ICON				"default-icon"

#define	STATUS_BAR_FONT				"status-bar-font"
#define	STATUS_BAR_BG				"status-bar-bg"
#define	STATUS_BAR_CONTAINER_BG		"status-bar-container-bg"
#define	STATUS_BAR_CONTAINER_X		"status-bar-container-pos-x"
#define	STATUS_BAR_CONTAINER_Y		"status-bar-container-pos-y"
#define	STATUS_BAR_CONTAINER_WIDTH	"status-bar-container-width"
#define	STATUS_BAR_CONTAINER_HEIGHT	"status-bar-container-height"

#define	EXPANDED_BG_X				"expanded-bg-x"
#define	EXPANDED_BG_Y				"expanded-bg-y"
#define	EXPANDED_BG_WIDTH			"expanded-bg-width"
#define	EXPANDED_BG_HEIGHT			"expanded-bg-height"
#define	EXPANDED_BG_IMG				"exapanded-background"

#define	LEFT_ARROW_IMG				"status-bar-left-arrow"
#define	RIGHT_ARROW_IMG				"status-bar-right-arrow"
#define	LEFT_ARROW_X				"status-bar-left-arrow-x"
#define	RIGHT_ARROW_X				"status-bar-right-arrow-x"
#define	STATUS_BAR_ARROW_Y			"status-bar-arrow-y"

#define	STATUS_BAR_WIDTH			"status-bar-width"
#define	STATUS_BAR_HEIGHT			"status-bar-height"

#define	DIVIDER_LINE_WIDTH			"divider-line-width"
#define	DIVIDER_LINE_HEIGHT			"divider-line-height"
#define	LEFT_DIVIDER_LINE_X			"left-divider-line-x"
#define	RIGHT_DIVIDER_LINE_X		"right-divider-line-x"
#define	FIRST_DIVIDER_LINE_X		"first-divider-line-x"
#define	DIVIDER_LINE_Y				"divider-line-y"

#define	EXPANDED_VIEW_WIDTH			"expanded-view-width"

#define	TIME_X_POS					"time-x-pos"
#define	TIME_Y_POS					"time-y-pos"
#define	TEXT_X_POS					"text-x-pos"
#define	TEXT_Y_POS					"text-y-pos"
#define	TEXT_WIDTH					"text-width"
#define	TEXT_HEIGHT					"text-height"

#define	ICON_WIDTH					"icon-width"
#define	ICON_HEIGHT					"icon-height"
#define	FIRST_ICON_X				"first-icon-x"
#define	FIRST_ICON_Y				"first-icon-y"

#define	TEXTURE_DIST_DIFF			"texture-dist-diff"
#define	DIVIDER_LINE_DIST_DIFF		"divider-line-dist-diff"

static void v_status_bar_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData);
static void v_status_bar_add_style_to_hash(StatusBar *statusBar, gchar *pKey, gpointer pValue);

static gboolean update_status_bar_clock (gpointer data);
static gboolean on_button_press(ClutterActor *actor, ClutterEvent *event, gpointer userData);
static gboolean on_button_release(ClutterActor *actor, ClutterEvent *event, gpointer userData);

static gboolean statusbar_show_status_info(gpointer userData);
static void show_shutdown_popup(gpointer userData);
static void second_texture_release(gpointer userData,gchar *name);
static void third_texture_release(gpointer userData,gchar *name);
static void fourth_texture_release(gpointer userData,gchar *name);
static void fifth_texture_release(gpointer userData,gchar *name);
static void sixth_texture_release(gpointer userData,gchar *name);
static void left_arrow_released(gpointer userData,gchar *name);
static void right_arrow_released(gpointer userData,gchar *name);
static void animate_right_side(gpointer userData);
static void animate_left_side(gpointer userData);
static void create_status_info_actors(StatusBar *status_bar);
static void create_status_bar_textures(StatusBar *status_bar);
static void create_status_bar_divider_lines(StatusBar *status_bar);
static void create_network_umts_icon_actors(StatusBar *status_bar);
static gboolean touch_event(ClutterActor *actor, ClutterEvent *event, gpointer userData);
static gboolean launch_app_touch_event(ClutterActor *actor, ClutterEvent *event, gpointer userData);

static void status_bar_get_property (GObject *object,guint property_id,GValue *value,GParamSpec *pspec)
{
	switch (property_id)
	{
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void status_bar_set_property (GObject *object,guint property_id,const GValue *value,GParamSpec *pspec)
{
	switch (property_id)
	{
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void status_bar_dispose (GObject *object)
{
	StatusBarPrivate *priv = BAR_PRIVATE (STATUS_BAR(object));
	g_clear_object (&priv->entry_point_index);
	G_OBJECT_CLASS (status_bar_parent_class)->dispose (object);
}

static void status_bar_finalize (GObject *object)
{
	G_OBJECT_CLASS (status_bar_parent_class)->finalize (object);
}


static gboolean dark_on_button_press(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	//g_print("\n Dark on button press \n");
	return TRUE;
}

static void
activate_async_cb (GObject *source_object,
                   GAsyncResult *result,
                   gpointer nil G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;
  CbyEntryPoint *entry_point = CBY_ENTRY_POINT (source_object);
  const gchar *id = cby_entry_point_get_id (entry_point);

  if (cby_entry_point_activate_finish (entry_point, result, &error))
    DEBUG ("Launched \"%s\"", id);
  else
    WARNING ("Unable to launch \"%s\": %s", id, error->message);
}

static gboolean dark_on_button_release(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
  StatusBar *self = STATUS_BAR (userData);
  const gchar *id;
  g_autoptr (CbyEntryPoint) entry_point = NULL;

  id = g_list_nth_data (self->priv->appList, self->priv->expandedTexture - 1);
  entry_point =
    cby_entry_point_index_get_by_id (self->priv->entry_point_index, id);

  if (entry_point != NULL)
    cby_entry_point_activate_async (entry_point, NULL, NULL, activate_async_cb,
                                    NULL);
  else
    WARNING ("Trying to launch entry point \"%s\" but it was not found", id);

  return TRUE;  /* handled */
}

static gboolean touch_event(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	//MildenhallProgressBarPrivate *priv = mildenhall_PROGRESS_BAR(userData)->priv;

	switch(event->type)
	{
	case CLUTTER_BUTTON_PRESS:
	case CLUTTER_TOUCH_BEGIN:
		on_button_press(actor,event,userData);
		break;

	case CLUTTER_BUTTON_RELEASE:
	case CLUTTER_TOUCH_END:
		on_button_release(actor,event,userData);
		break;

	default:
		return FALSE;
		break;

	}
	return TRUE;
}

static gboolean launch_app_touch_event(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	//MildenhallProgressBarPrivate *priv = mildenhall_PROGRESS_BAR(userData)->priv;

	switch(event->type)
	{
	case CLUTTER_BUTTON_PRESS:
	case CLUTTER_TOUCH_BEGIN:
		dark_on_button_press(actor,event,userData);
		break;

	case CLUTTER_BUTTON_RELEASE:
	case CLUTTER_TOUCH_END:
		dark_on_button_release(actor,event,userData);
		break;

	default:
		return FALSE;
		break;

	}
	return TRUE;
}



static void create_status_info_actors(StatusBar *status_bar)
{
	ClutterColor time_color = { 0xB8, 0xCB, 0xD1, 0xB3 };
	gint64 textX,textY,textWidth,textHeight;
	gint64 textureDistDiff;
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,TEXT_X_POS), "%" G_GINT64_FORMAT, &textX);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,TEXT_Y_POS), "%" G_GINT64_FORMAT, &textY);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,TEXT_WIDTH), "%" G_GINT64_FORMAT, &textWidth);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,TEXT_HEIGHT), "%" G_GINT64_FORMAT, &textHeight);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,TEXTURE_DIST_DIFF), "%" G_GINT64_FORMAT, &textureDistDiff);
	gchar *defaultText = (gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,DEFAULT_TEXT);
	gchar *statusBarFont = (gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_FONT);
	status_bar->priv->statusInfo1 =  clutter_text_new_full(statusBarFont,defaultText,&time_color);
	clutter_actor_set_size (status_bar->priv->statusInfo1,textWidth,textHeight);
	clutter_actor_set_position(status_bar->priv->statusInfo1,textX,7);
	clutter_text_set_ellipsize(CLUTTER_TEXT(status_bar->priv->statusInfo1),PANGO_ELLIPSIZE_END);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->statusInfo1);
	status_bar->priv->statusInfoList=g_list_append(status_bar->priv->statusInfoList,(gpointer)status_bar->priv->statusInfo1);
	g_signal_connect (status_bar->priv->statusInfo1, "button-press-event", G_CALLBACK(dark_on_button_press), status_bar);
	g_signal_connect (status_bar->priv->statusInfo1, "button-release-event", G_CALLBACK(dark_on_button_release), status_bar);
	g_signal_connect (status_bar->priv->statusInfo1, "touch-event", G_CALLBACK(launch_app_touch_event), status_bar);

	status_bar->priv->statusInfo2 =  clutter_text_new_full(statusBarFont,defaultText,&time_color);
	clutter_actor_set_size (status_bar->priv->statusInfo2,textWidth,textHeight);
	clutter_actor_set_position(status_bar->priv->statusInfo2,textX+(textureDistDiff*1),7);
	clutter_text_set_ellipsize(CLUTTER_TEXT(status_bar->priv->statusInfo2),PANGO_ELLIPSIZE_END);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->statusInfo2);
	status_bar->priv->statusInfoList=g_list_append(status_bar->priv->statusInfoList,(gpointer)status_bar->priv->statusInfo2);
	clutter_actor_hide(status_bar->priv->statusInfo2);

	status_bar->priv->statusInfo3 =  clutter_text_new_full(statusBarFont,defaultText,&time_color);
	clutter_actor_set_size (status_bar->priv->statusInfo3,textWidth,textHeight);
	clutter_actor_set_position(status_bar->priv->statusInfo3,textX+(textureDistDiff*2),7);
	clutter_text_set_ellipsize(CLUTTER_TEXT(status_bar->priv->statusInfo3),PANGO_ELLIPSIZE_END);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->statusInfo3);
	status_bar->priv->statusInfoList=g_list_append(status_bar->priv->statusInfoList,(gpointer)status_bar->priv->statusInfo3);
	clutter_actor_hide(status_bar->priv->statusInfo3);

	status_bar->priv->statusInfo4 =  clutter_text_new_full(statusBarFont,defaultText,&time_color);
	clutter_actor_set_size (status_bar->priv->statusInfo4,textWidth,textHeight);
	clutter_actor_set_position(status_bar->priv->statusInfo4,textX+(textureDistDiff*3),7);
	clutter_text_set_ellipsize(CLUTTER_TEXT(status_bar->priv->statusInfo4),PANGO_ELLIPSIZE_END);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->statusInfo4);
	status_bar->priv->statusInfoList=g_list_append(status_bar->priv->statusInfoList,(gpointer)status_bar->priv->statusInfo4);
	clutter_actor_hide(status_bar->priv->statusInfo4);

	status_bar->priv->statusInfo5 =  clutter_text_new_full(statusBarFont,defaultText,&time_color);
	clutter_actor_set_size (status_bar->priv->statusInfo5,textWidth,textHeight);
	clutter_actor_set_position(status_bar->priv->statusInfo5,textX+(textureDistDiff*4),7);
	clutter_text_set_ellipsize(CLUTTER_TEXT(status_bar->priv->statusInfo5),PANGO_ELLIPSIZE_END);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->statusInfo5);
	status_bar->priv->statusInfoList=g_list_append(status_bar->priv->statusInfoList,(gpointer)status_bar->priv->statusInfo5);
	clutter_actor_hide(status_bar->priv->statusInfo5);

	status_bar->priv->statusInfo6 =  clutter_text_new_full(statusBarFont,defaultText,&time_color);
	clutter_actor_set_size (status_bar->priv->statusInfo6,textWidth,textHeight);
	clutter_actor_set_position(status_bar->priv->statusInfo6,textX+(textureDistDiff*5),7);
	clutter_text_set_ellipsize(CLUTTER_TEXT(status_bar->priv->statusInfo6),PANGO_ELLIPSIZE_END);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->statusInfo6);
	status_bar->priv->statusInfoList=g_list_append(status_bar->priv->statusInfoList,(gpointer)status_bar->priv->statusInfo6);
	clutter_actor_hide(status_bar->priv->statusInfo6);
}



static void create_status_bar_textures(StatusBar *status_bar)
{
	gint64 iconWidth,iconHeight,iconX,iconY;
	gint64 textureDistDiff;
	gint64 expandedViewWidth;
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,EXPANDED_VIEW_WIDTH), "%" G_GINT64_FORMAT, &expandedViewWidth);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,ICON_WIDTH), "%" G_GINT64_FORMAT, &iconWidth);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,ICON_HEIGHT), "%" G_GINT64_FORMAT, &iconHeight);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,FIRST_ICON_X), "%" G_GINT64_FORMAT, &iconX);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,FIRST_ICON_Y), "%" G_GINT64_FORMAT, &iconY);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,TEXTURE_DIST_DIFF), "%" G_GINT64_FORMAT, &textureDistDiff);
	gchar *defaultIcon = (gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,DEFAULT_ICON);
	defaultIcon = g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,"/",defaultIcon,NULL);

	status_bar->priv->textureOne = thornbury_ui_texture_create_new(defaultIcon,64,64,FALSE,FALSE);
	clutter_actor_set_size (status_bar->priv->textureOne, iconWidth, iconHeight);
	clutter_actor_set_position(status_bar->priv->textureOne,45,-4);//iconX,iconY);
	clutter_actor_set_reactive(status_bar->priv->textureOne,FALSE);
	clutter_actor_set_name(status_bar->priv->textureOne,"textureOne");
	g_signal_connect (status_bar->priv->textureOne, "button-press-event", G_CALLBACK(on_button_press), status_bar);
	g_signal_connect (status_bar->priv->textureOne, "button-release-event", G_CALLBACK(on_button_release), status_bar);
	g_signal_connect (status_bar->priv->textureOne, "touch-event", G_CALLBACK(touch_event), status_bar);
	status_bar->priv->textureList=g_list_append(status_bar->priv->textureList,(gpointer)status_bar->priv->textureOne);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->textureOne);

	status_bar->priv->textureTwo = thornbury_ui_texture_create_new(defaultIcon,0,0,FALSE,FALSE);
	clutter_actor_set_size (status_bar->priv->textureTwo, iconWidth, iconHeight);
	clutter_actor_set_position(status_bar->priv->textureTwo,iconX+(textureDistDiff*1)+expandedViewWidth,iconY);
	clutter_actor_set_reactive(status_bar->priv->textureTwo,TRUE);
	clutter_actor_set_name(status_bar->priv->textureTwo,"textureTwo");
	g_signal_connect (status_bar->priv->textureTwo, "button-press-event", G_CALLBACK(on_button_press), status_bar);
	g_signal_connect (status_bar->priv->textureTwo, "button-release-event", G_CALLBACK(on_button_release), status_bar);
	g_signal_connect (status_bar->priv->textureTwo, "touch-event", G_CALLBACK(touch_event), status_bar);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->textureTwo);
	status_bar->priv->textureList=g_list_append(status_bar->priv->textureList,(gpointer)status_bar->priv->textureTwo);
	clutter_actor_hide(status_bar->priv->textureTwo);

	status_bar->priv->textureThree = thornbury_ui_texture_create_new(defaultIcon,0,0,FALSE,FALSE);
	clutter_actor_set_size (status_bar->priv->textureThree, iconWidth, iconHeight);
	clutter_actor_set_position(status_bar->priv->textureThree,iconX+(textureDistDiff*2)+expandedViewWidth,iconY);
	clutter_actor_set_reactive(status_bar->priv->textureThree,TRUE);
	clutter_actor_set_name(status_bar->priv->textureThree,"textureThree");
	g_signal_connect (status_bar->priv->textureThree, "button-press-event", G_CALLBACK(on_button_press), status_bar);
	g_signal_connect (status_bar->priv->textureThree, "button-release-event", G_CALLBACK(on_button_release), status_bar);
	g_signal_connect (status_bar->priv->textureThree, "touch-event", G_CALLBACK(touch_event), status_bar);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->textureThree);
	status_bar->priv->textureList=g_list_append(status_bar->priv->textureList,(gpointer)status_bar->priv->textureThree);
	clutter_actor_hide(status_bar->priv->textureThree);

	status_bar->priv->textureFour = thornbury_ui_texture_create_new(defaultIcon,0,0,FALSE,FALSE);
	clutter_actor_set_size (status_bar->priv->textureFour, iconWidth, iconHeight);
	clutter_actor_set_position(status_bar->priv->textureFour,iconX+(textureDistDiff*3)+expandedViewWidth,iconY);
	clutter_actor_set_reactive(status_bar->priv->textureFour,TRUE);
	clutter_actor_set_name(status_bar->priv->textureFour,"textureFour");
	g_signal_connect (status_bar->priv->textureFour, "button-press-event", G_CALLBACK(on_button_press), status_bar);
	g_signal_connect (status_bar->priv->textureFour, "button-release-event", G_CALLBACK(on_button_release), status_bar);
	g_signal_connect (status_bar->priv->textureFour, "touch-event", G_CALLBACK(touch_event), status_bar);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->textureFour);
	status_bar->priv->textureList=g_list_append(status_bar->priv->textureList,(gpointer)status_bar->priv->textureFour);
	clutter_actor_hide(status_bar->priv->textureFour);

	status_bar->priv->textureFive = thornbury_ui_texture_create_new(defaultIcon,0,0,FALSE,FALSE);
	clutter_actor_set_size (status_bar->priv->textureFive, iconWidth, iconHeight);
	clutter_actor_set_position(status_bar->priv->textureFive,iconX+(textureDistDiff*4)+expandedViewWidth,iconY);
	clutter_actor_set_reactive(status_bar->priv->textureFive,TRUE);
	clutter_actor_set_name(status_bar->priv->textureFive,"textureFive");
	g_signal_connect (status_bar->priv->textureFive, "button-press-event", G_CALLBACK(on_button_press), status_bar);
	g_signal_connect (status_bar->priv->textureFive, "button-release-event", G_CALLBACK(on_button_release), status_bar);
	g_signal_connect (status_bar->priv->textureFive, "touch-event", G_CALLBACK(touch_event), status_bar);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->textureFive);
	status_bar->priv->textureList=g_list_append(status_bar->priv->textureList,(gpointer)status_bar->priv->textureFive);
	clutter_actor_hide(status_bar->priv->textureFive);

	status_bar->priv->textureSix = thornbury_ui_texture_create_new(defaultIcon,0,0,FALSE,FALSE);
	clutter_actor_set_size (status_bar->priv->textureSix, iconWidth, iconHeight);
	clutter_actor_set_position(status_bar->priv->textureSix,iconX+(textureDistDiff*5)+expandedViewWidth,iconY);
	clutter_actor_set_reactive(status_bar->priv->textureSix,TRUE);
	clutter_actor_set_name(status_bar->priv->textureSix,"textureSix");
	g_signal_connect (status_bar->priv->textureSix, "button-press-event", G_CALLBACK(on_button_press), status_bar);
	g_signal_connect (status_bar->priv->textureSix, "button-release-event", G_CALLBACK(on_button_release), status_bar);
	g_signal_connect (status_bar->priv->textureSix, "touch-event", G_CALLBACK(touch_event), status_bar);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->textureSix);
	status_bar->priv->textureList=g_list_append(status_bar->priv->textureList,(gpointer)status_bar->priv->textureSix);
	clutter_actor_hide(status_bar->priv->textureSix);

}

static void create_status_bar_divider_lines(StatusBar *status_bar)
{
	gint64 leftarrowX,rightArrowX,arrowY;
	ClutterColor time_color = { 0xB8, 0xCB, 0xD1, 0xB3 };
	gint64 dividerLineWidth,dividerLineHeight,leftDividerLineX,rightDividerLineX,firstDividerLineX,dividerLineY;
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,LEFT_ARROW_X), "%" G_GINT64_FORMAT, &leftarrowX);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,RIGHT_ARROW_X), "%" G_GINT64_FORMAT, &rightArrowX);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_ARROW_Y), "%" G_GINT64_FORMAT, &arrowY);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,DIVIDER_LINE_WIDTH), "%" G_GINT64_FORMAT, &dividerLineWidth);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,DIVIDER_LINE_HEIGHT), "%" G_GINT64_FORMAT, &dividerLineHeight);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,LEFT_DIVIDER_LINE_X), "%" G_GINT64_FORMAT, &leftDividerLineX);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,RIGHT_DIVIDER_LINE_X), "%" G_GINT64_FORMAT, &rightDividerLineX);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,FIRST_DIVIDER_LINE_X), "%" G_GINT64_FORMAT, &firstDividerLineX);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,DIVIDER_LINE_Y), "%" G_GINT64_FORMAT, &dividerLineY);
	gint64 expandedViewWidth;
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,EXPANDED_VIEW_WIDTH), "%" G_GINT64_FORMAT, &expandedViewWidth);
	gint64 dividerLineDistDiff;
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,DIVIDER_LINE_DIST_DIFF), "%" G_GINT64_FORMAT, &dividerLineDistDiff);
	gchar *leftArrowImage = (gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,LEFT_ARROW_IMG);
	leftArrowImage = g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,"/",leftArrowImage,NULL);
	gchar *rightArrowImage = (gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,RIGHT_ARROW_IMG);
	rightArrowImage = g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,"/",rightArrowImage,NULL);

	status_bar->priv->leftDividerLine = clutter_actor_new();
	clutter_actor_set_size(status_bar->priv->leftDividerLine,dividerLineWidth,dividerLineHeight);
	clutter_actor_set_position(status_bar->priv->leftDividerLine,leftDividerLineX,dividerLineY);
	clutter_actor_set_background_color(status_bar->priv->leftDividerLine,&time_color);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->leftDividerLine);

	status_bar->priv->rightDividerLine = clutter_actor_new();
	clutter_actor_set_size(status_bar->priv->rightDividerLine,dividerLineWidth,dividerLineHeight);
	clutter_actor_set_position(status_bar->priv->rightDividerLine,rightDividerLineX,dividerLineY);
	clutter_actor_set_background_color(status_bar->priv->rightDividerLine,&time_color);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->rightDividerLine);


	status_bar->priv->dividerLineOne = clutter_actor_new();
	clutter_actor_set_size(status_bar->priv->dividerLineOne,dividerLineWidth,dividerLineHeight);
	clutter_actor_set_position(status_bar->priv->dividerLineOne,firstDividerLineX+expandedViewWidth,dividerLineY);
	clutter_actor_set_background_color(status_bar->priv->dividerLineOne,&time_color);
	status_bar->priv->dividerLineList=g_list_append(status_bar->priv->dividerLineList,(gpointer)status_bar->priv->dividerLineOne);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->dividerLineOne);
	clutter_actor_hide(status_bar->priv->dividerLineOne);


	status_bar->priv->dividerLineTwo = clutter_actor_new();
	clutter_actor_set_size(status_bar->priv->dividerLineTwo,dividerLineWidth,dividerLineHeight);
	clutter_actor_set_position(status_bar->priv->dividerLineTwo,firstDividerLineX+(dividerLineDistDiff*1)+expandedViewWidth,dividerLineY);
	clutter_actor_set_background_color(status_bar->priv->dividerLineTwo,&time_color);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->dividerLineTwo);
	status_bar->priv->dividerLineList=g_list_append(status_bar->priv->dividerLineList,(gpointer)status_bar->priv->dividerLineTwo);
	clutter_actor_hide(status_bar->priv->dividerLineTwo);

	status_bar->priv->dividerLineThree = clutter_actor_new();
	clutter_actor_set_size(status_bar->priv->dividerLineThree,dividerLineWidth,dividerLineHeight);
	clutter_actor_set_position(status_bar->priv->dividerLineThree,firstDividerLineX+(dividerLineDistDiff*2)+expandedViewWidth,dividerLineY);
	clutter_actor_set_background_color(status_bar->priv->dividerLineThree,&time_color);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->dividerLineThree);
	status_bar->priv->dividerLineList=g_list_append(status_bar->priv->dividerLineList,(gpointer)status_bar->priv->dividerLineThree);
	clutter_actor_hide(status_bar->priv->dividerLineThree);

	status_bar->priv->dividerLineFour = clutter_actor_new();
	clutter_actor_set_size(status_bar->priv->dividerLineFour,dividerLineWidth,dividerLineHeight);
	clutter_actor_set_position(status_bar->priv->dividerLineFour,firstDividerLineX+(dividerLineDistDiff*3)+expandedViewWidth,dividerLineY);
	clutter_actor_set_background_color(status_bar->priv->dividerLineFour,&time_color);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->dividerLineFour);
	status_bar->priv->dividerLineList=g_list_append(status_bar->priv->dividerLineList,(gpointer)status_bar->priv->dividerLineFour);
	clutter_actor_hide(status_bar->priv->dividerLineFour);

	status_bar->priv->dividerLineFive = clutter_actor_new();
	clutter_actor_set_size(status_bar->priv->dividerLineFive,dividerLineWidth,dividerLineHeight);
	clutter_actor_set_position(status_bar->priv->dividerLineFive,firstDividerLineX+(dividerLineDistDiff*4)+expandedViewWidth,dividerLineY);
	clutter_actor_set_background_color(status_bar->priv->dividerLineFive,&time_color);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->dividerLineFive);
	status_bar->priv->dividerLineList=g_list_append(status_bar->priv->dividerLineList,(gpointer)status_bar->priv->dividerLineFive);
	clutter_actor_hide(status_bar->priv->dividerLineFive);

	status_bar->priv->dividerLineSix = clutter_actor_new();
	clutter_actor_set_size(status_bar->priv->dividerLineSix,dividerLineWidth,dividerLineHeight);
	clutter_actor_set_position(status_bar->priv->dividerLineSix,firstDividerLineX+(dividerLineDistDiff*5)+expandedViewWidth,dividerLineY);
	clutter_actor_set_background_color(status_bar->priv->dividerLineSix,&time_color);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->dividerLineSix);
	status_bar->priv->dividerLineList=g_list_append(status_bar->priv->dividerLineList,(gpointer)status_bar->priv->dividerLineSix);
	clutter_actor_hide(status_bar->priv->dividerLineSix);
}

static void create_network_umts_icon_actors(StatusBar *status_bar)
{
	gchar *networkIcon = g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,"/Network_icon.png",NULL);
	status_bar->priv->networkIcon = thornbury_ui_texture_create_new(networkIcon,0,0,FALSE,FALSE);
	clutter_actor_set_size(status_bar->priv->networkIcon,50,40);
	clutter_actor_set_position(status_bar->priv->networkIcon,8,9);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar->priv->statusBarBg),status_bar->priv->networkIcon);

	gchar *umtsIcon = g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,"/icon_SignalStrength_0.png",NULL);
	status_bar->priv->umtsIcon = thornbury_ui_texture_create_new(umtsIcon,55,20,FALSE,FALSE);
	clutter_actor_set_position(status_bar->priv->umtsIcon,735,17);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar->priv->statusBarBg),status_bar->priv->umtsIcon);
	clutter_actor_set_reactive(status_bar->priv->umtsIcon,TRUE);
	clutter_actor_set_name(status_bar->priv->umtsIcon,"shutdown");
	g_signal_connect (status_bar->priv->umtsIcon, "button-release-event", G_CALLBACK(on_button_release), status_bar);
	g_signal_connect (status_bar->priv->umtsIcon, "touch-event", G_CALLBACK(touch_event), status_bar);

}
/********************************************************
 * Function : mildenhall_status_bar_create
 * Description: Creates the default status bar.
 * Parameter :
 * Return value: ClutterActor
 ********************************************************/
ClutterActor *mildenhall_status_bar_create(gchar *pAppName)
{
	StatusBar *status_bar = g_object_new(STATUS_TYPE_BAR, NULL);

	status_bar->priv->mildenhall_STATUS_BAR_APP_NAME = g_strdup(pAppName);
	status_bar->priv->expandedTexture=1;
	status_bar->priv->onClicked = 1;
	status_bar->priv->pIconData = NULL;
	status_bar->priv->pStatusData = NULL;
	ClutterColor time_color = { 0xB8, 0xCB, 0xD1, 0xB3 };
	gint64 containerX,containerY,containerWidth,containerHeight;
	gint64 expandedBgx,expandedBgy,expandedBgWidth,expandedBgHeight;
	gint64 statusBarWidth,statusBarHeight;
	gint64 timeX,timeY;
	gint64 leftarrowX,rightArrowX,arrowY;

	gchar *statusBarFont = (gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_FONT);
	gchar *statusBarBackground = (gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_BG);
	statusBarBackground = g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,"/",statusBarBackground,NULL);
	gchar *statusBarContainerBackgournd = (gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_CONTAINER_BG);
	statusBarContainerBackgournd = g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,"/",statusBarContainerBackgournd,NULL);
	gchar *expandedBackground = (gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,EXPANDED_BG_IMG);
	expandedBackground = g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,"/",expandedBackground,NULL);
	gchar *leftArrowImage = (gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,LEFT_ARROW_IMG);
	leftArrowImage = g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,"/",leftArrowImage,NULL);
	gchar *rightArrowImage = (gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,RIGHT_ARROW_IMG);
	rightArrowImage = g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,"/",rightArrowImage,NULL);

	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_CONTAINER_X), "%" G_GINT64_FORMAT, &containerX);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_CONTAINER_Y), "%" G_GINT64_FORMAT, &containerY);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_CONTAINER_WIDTH), "%" G_GINT64_FORMAT, &containerWidth);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_CONTAINER_HEIGHT), "%" G_GINT64_FORMAT, &containerHeight);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,EXPANDED_BG_X), "%" G_GINT64_FORMAT, &expandedBgx);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,EXPANDED_BG_Y), "%" G_GINT64_FORMAT, &expandedBgy);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,EXPANDED_BG_WIDTH), "%" G_GINT64_FORMAT, &expandedBgWidth);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,EXPANDED_BG_HEIGHT), "%" G_GINT64_FORMAT, &expandedBgHeight);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_WIDTH), "%" G_GINT64_FORMAT, &statusBarWidth);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_HEIGHT), "%" G_GINT64_FORMAT, &statusBarHeight);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,TIME_X_POS), "%" G_GINT64_FORMAT, &timeX);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,TIME_Y_POS), "%" G_GINT64_FORMAT, &timeY);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,LEFT_ARROW_X), "%" G_GINT64_FORMAT, &leftarrowX);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,RIGHT_ARROW_X), "%" G_GINT64_FORMAT, &rightArrowX);
	sscanf((gchar*) g_hash_table_lookup(status_bar->priv->pLocalHash,STATUS_BAR_ARROW_Y), "%" G_GINT64_FORMAT, &arrowY);

	status_bar->priv->statusBarBg = thornbury_ui_texture_create_new (statusBarBackground, 0,0,FALSE,FALSE);
	clutter_actor_set_size (status_bar->priv->statusBarBg,statusBarWidth, statusBarHeight);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar),status_bar->priv->statusBarBg);

	status_bar->priv->statusBarContainer = thornbury_ui_texture_create_new(statusBarContainerBackgournd,0,0,FALSE,FALSE);
	clutter_actor_set_size(status_bar->priv->statusBarContainer,containerWidth,containerHeight);
	clutter_actor_set_position(status_bar->priv->statusBarContainer,containerX,containerY);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar->priv->statusBarBg),status_bar->priv->statusBarContainer);

	status_bar->priv->darkImage = thornbury_ui_texture_create_new(expandedBackground,0,0,FALSE,FALSE);
	clutter_actor_set_size (status_bar->priv->darkImage, expandedBgWidth, expandedBgHeight);
	clutter_actor_set_position(status_bar->priv->darkImage,expandedBgx,expandedBgy);
	clutter_actor_set_reactive(status_bar->priv->darkImage,TRUE);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->darkImage);

	g_signal_connect (status_bar->priv->darkImage, "button-press-event", G_CALLBACK(dark_on_button_press), status_bar);
	g_signal_connect (status_bar->priv->darkImage, "button-release-event", G_CALLBACK(dark_on_button_release), status_bar);
	g_signal_connect (status_bar->priv->darkImage, "touch-event", G_CALLBACK(launch_app_touch_event), status_bar);

	struct tm *t;
	time_t *ts = (time_t *) malloc (sizeof (time_t));
	*ts = time (NULL);
	t = localtime (ts);
	gchar *time = g_strdup_printf ("%02d:%02d", t->tm_hour, t->tm_min);

	status_bar->priv->statusBarTime=clutter_text_new_full(statusBarFont,time,&time_color);
	clutter_actor_set_position(status_bar->priv->statusBarTime,timeX,timeY);
	g_timeout_add (MILLI_SECOND, update_status_bar_clock, (gpointer) status_bar->priv->statusBarTime);
	clutter_actor_add_child (CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->statusBarTime);

	status_bar->priv->leftArrowTexture=thornbury_ui_texture_create_new(leftArrowImage,0,0,FALSE,FALSE);
	clutter_actor_set_position(status_bar->priv->leftArrowTexture,leftarrowX,arrowY);
	clutter_actor_set_name(status_bar->priv->leftArrowTexture,"LeftArrow");
	clutter_actor_set_reactive(status_bar->priv->leftArrowTexture,TRUE);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->leftArrowTexture);
	g_signal_connect (status_bar->priv->leftArrowTexture, "button-press-event", G_CALLBACK(on_button_press), status_bar);
	g_signal_connect (status_bar->priv->leftArrowTexture, "button-release-event", G_CALLBACK(on_button_release), status_bar);
	g_signal_connect (status_bar->priv->leftArrowTexture, "touch-event", G_CALLBACK(touch_event), status_bar);
	clutter_actor_hide(status_bar->priv->leftArrowTexture);

	status_bar->priv->rightArrowTexture=thornbury_ui_texture_create_new(rightArrowImage,0,0,FALSE,FALSE);
	clutter_actor_set_position(status_bar->priv->rightArrowTexture,rightArrowX,arrowY);
	clutter_actor_set_name(status_bar->priv->rightArrowTexture,"RightArrow");
	clutter_actor_set_reactive(status_bar->priv->rightArrowTexture,TRUE);
	clutter_actor_add_child(CLUTTER_ACTOR(status_bar->priv->statusBarContainer),status_bar->priv->rightArrowTexture);
	g_signal_connect (status_bar->priv->rightArrowTexture, "button-press-event", G_CALLBACK(on_button_press), status_bar);
	g_signal_connect (status_bar->priv->rightArrowTexture, "button-release-event", G_CALLBACK(on_button_release), status_bar);
	g_signal_connect (status_bar->priv->rightArrowTexture, "touch-event", G_CALLBACK(touch_event), status_bar);
	clutter_actor_hide(status_bar->priv->rightArrowTexture);

	create_status_info_actors(status_bar);
	create_status_bar_textures(status_bar);
	create_status_bar_divider_lines(status_bar);
	create_network_umts_icon_actors(status_bar);

	return CLUTTER_ACTOR(status_bar);

}



static gboolean statusbar_show_status_info(gpointer userData)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	//g_print("\n Showing the status info actor = %d",(statusBar->priv->expandedTexture-1));
	clutter_actor_show(CLUTTER_ACTOR(g_list_nth_data(statusBar->priv->statusInfoList,(statusBar->priv->expandedTexture-1))));
	return FALSE;
}



static void second_texture_release(gpointer userData,gchar *name)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	statusBar->priv->onClicked = 2;
	update_status_bar((GObject *)statusBar,name);
	statusBar->priv->expandedTexture=2;
	clutter_actor_set_reactive(statusBar->priv->textureOne,TRUE);
	hide_or_show_actors(userData);
	if(statusBar->priv->expandedTexture == g_list_length(statusBar->priv->appList))
	{
		clutter_actor_hide(statusBar->priv->rightArrowTexture);
		clutter_actor_show(statusBar->priv->leftArrowTexture);
	}
	else if(statusBar->priv->expandedTexture < g_list_length(statusBar->priv->appList))
	{
		clutter_actor_show(statusBar->priv->leftArrowTexture);
		clutter_actor_show(statusBar->priv->rightArrowTexture);
	}
}
static void third_texture_release(gpointer userData,gchar *name)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	statusBar->priv->onClicked = 3;
	update_status_bar((GObject *)statusBar,name);
	statusBar->priv->expandedTexture=3;
	hide_or_show_actors(userData);
	if(statusBar->priv->expandedTexture == g_list_length(statusBar->priv->appList))
	{
		clutter_actor_hide(statusBar->priv->rightArrowTexture);
		clutter_actor_show(statusBar->priv->leftArrowTexture);
	}
	else if(statusBar->priv->expandedTexture < g_list_length(statusBar->priv->appList))
	{
		clutter_actor_show(statusBar->priv->leftArrowTexture);
		clutter_actor_show(statusBar->priv->rightArrowTexture);
	}

}
static void fourth_texture_release(gpointer userData,gchar *name)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	statusBar->priv->onClicked = 4;
	update_status_bar((GObject *)statusBar,name);
	statusBar->priv->expandedTexture=4;
	hide_or_show_actors(userData);
	if(statusBar->priv->expandedTexture == g_list_length(statusBar->priv->appList))
	{
		clutter_actor_hide(statusBar->priv->rightArrowTexture);
		clutter_actor_show(statusBar->priv->leftArrowTexture);
	}
	else if(statusBar->priv->expandedTexture < g_list_length(statusBar->priv->appList))
	{
		clutter_actor_show(statusBar->priv->leftArrowTexture);
		clutter_actor_show(statusBar->priv->rightArrowTexture);
	}
}
static void fifth_texture_release(gpointer userData,gchar *name)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	statusBar->priv->onClicked = 5;
	update_status_bar((GObject *)statusBar,name);
	statusBar->priv->expandedTexture=5;
	hide_or_show_actors(userData);
	if(statusBar->priv->expandedTexture == g_list_length(statusBar->priv->appList))
	{
		clutter_actor_hide(statusBar->priv->rightArrowTexture);
		clutter_actor_show(statusBar->priv->leftArrowTexture);
	}
	else if(statusBar->priv->expandedTexture < g_list_length(statusBar->priv->appList))
	{
		clutter_actor_show(statusBar->priv->leftArrowTexture);
		clutter_actor_show(statusBar->priv->rightArrowTexture);
	}
}
static void sixth_texture_release(gpointer userData,gchar *name)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	statusBar->priv->onClicked = 6;
	update_status_bar((GObject *)statusBar,name);
	statusBar->priv->expandedTexture=6;
	clutter_actor_hide(statusBar->priv->rightArrowTexture);
	hide_or_show_actors(userData);
	clutter_actor_hide(statusBar->priv->rightArrowTexture);
	clutter_actor_show(statusBar->priv->leftArrowTexture);
}
static void right_arrow_released(gpointer userData,gchar *name)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	++statusBar->priv->onClicked;
	//status_bar_debug("\n In Right Arrow onClicked = %d \n",statusBar->priv->onClicked);
	if(statusBar->priv->onClicked <= g_list_length(statusBar->priv->appList))
	{
		//status_bar_debug("\n Inside if \n");
		update_status_bar((GObject *)statusBar,name);
		++statusBar->priv->expandedTexture;
		hide_or_show_actors(userData);
	}
	if(statusBar->priv->onClicked == g_list_length(statusBar->priv->appList))
		clutter_actor_hide(statusBar->priv->rightArrowTexture);
	if(statusBar->priv->expandedTexture > 1)
		clutter_actor_show(statusBar->priv->leftArrowTexture);
}
static void left_arrow_released(gpointer userData,gchar *name)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	--statusBar->priv->onClicked;
	if(statusBar->priv->onClicked >= 1 )
	{
		update_status_bar((GObject *)statusBar,name);
		--statusBar->priv->expandedTexture;
		hide_or_show_actors(userData);
	}
	if(statusBar->priv->onClicked == 1)
	{
		clutter_actor_hide(statusBar->priv->leftArrowTexture);
		clutter_actor_show(statusBar->priv->rightArrowTexture);
	}
	if(statusBar->priv->expandedTexture != g_list_length(statusBar->priv->appList))
		clutter_actor_show(statusBar->priv->rightArrowTexture);
}
/********************************************************
 * Function : on_button_release
 * Description: Callback function for the button-release signal.
 * Parameter :  *actor,*event,userData
 * Return value: void
 ********************************************************/
static gboolean on_button_release(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	gchar *name;
	name = (gchar *)clutter_actor_get_name(actor);

	if(!(g_strcmp0("textureOne",name)))
	{
		statusBar->priv->onClicked = 1;
		update_status_bar((GObject *)statusBar,name);
		statusBar->priv->expandedTexture=1;
		hide_or_show_actors(userData);
		clutter_actor_hide(statusBar->priv->leftArrowTexture);
		if(g_list_length(statusBar->priv->appList) > 1)
			clutter_actor_show(statusBar->priv->rightArrowTexture);
	}
	if(!(g_strcmp0("textureTwo",name)))
	{
		second_texture_release(userData,name);
	}

	if(!(g_strcmp0("textureThree",name)))
	{
		third_texture_release(userData,name);
	}
	if(!(g_strcmp0("textureFour",name)))
	{
		fourth_texture_release(userData,name);
	}
	if(!(g_strcmp0("textureFive",name)))
	{
		fifth_texture_release(userData,name);
	}
	if(!(g_strcmp0("textureSix",name)))
	{
		sixth_texture_release(userData,name);
	}

	if(!(g_strcmp0("RightArrow",name)))
	{
		right_arrow_released(userData,name);

	}
	if(!(g_strcmp0("LeftArrow",name)))
	{
		left_arrow_released(userData,name);
	}
	if(!(g_strcmp0("shutdown",name)))
	{
		show_shutdown_popup(userData);
	}
	return TRUE;
}

void hide_or_show_actors(gpointer userData)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	gint index;
	for(index = 0; index < g_list_length(statusBar->priv->statusInfoList);index++)
	{
		if(index == (statusBar->priv->expandedTexture-1))
		{
			//g_print("\n Showed Actor = %d",index);
			g_timeout_add(600,(GSourceFunc)statusbar_show_status_info,userData);
		}
		else
		{
			//g_print("\n Hided Actor = %d",index);
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(statusBar->priv->statusInfoList,index)));
		}

	}
}


/********************************************************
 * Function : show_shutdown_popup
 * Description: Show the shutdown popup
 * Parameter :  *object,*name
 * Return value: void
 ********************************************************/
static void show_shutdown_popup(gpointer userData)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	GVariantBuilder *pGvb = g_variant_builder_new ( G_VARIANT_TYPE ("a{ss}") );
	g_variant_builder_add (pGvb, "{ss}","Text-Active",g_strdup("P O W E R   O F F ?") );

	GVariant* pDisplay_Text = g_variant_builder_end (pGvb);
	GVariantBuilder *pGvb2 = g_variant_builder_new ( G_VARIANT_TYPE ("a{ss}") );
	g_variant_builder_add ( pGvb2, "{ss}","YES",MILDENHALL_POPUP_BUTTON_STRING_YES );
	g_variant_builder_add ( pGvb2, "{ss}","NO",MILDENHALL_POPUP_BUTTON_STRING_NO );
	GVariant* pPopup_action = g_variant_builder_end (pGvb2);
	GVariantBuilder *pGvb3 = g_variant_builder_new ( G_VARIANT_TYPE ("a{ss}") );

	gchar *pStatSettings = g_strconcat(statusBar->priv->PKG_DATA_DIRECTORY,"/status_settings.png",NULL);
	gchar *pPowerIcon = g_strconcat(statusBar->priv->PKG_DATA_DIRECTORY,"/Power.png",NULL);

	g_variant_builder_add ( pGvb3, "{ss}","AppIcon",pStatSettings);
	g_variant_builder_add ( pGvb3, "{ss}","MsgIcon",pPowerIcon);
	GVariant* IMAGE = g_variant_builder_end (pGvb3);

	barkway_service_call_show_popup(statusBar->priv->popup_proxy,statusBar->priv->mildenhall_STATUS_BAR_APP_NAME,BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_TWO_WITH_TEXT,"twobutton",pDisplay_Text,"HIGH",pPopup_action,
			IMAGE,-1,"NULL",3,NULL,NULL,NULL);

	status_bar_free_pkgdatadir_text(pStatSettings);
	status_bar_free_pkgdatadir_text(pPowerIcon);
}

static void animate_left_side(gpointer userData)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	int loopVariable=0;

	gint64 expandedBgx;
	gint64 firstDividerLineX;
	gint64 textX;
	gint64 iconX;
	gint64 textureDistDiff,dividerLineDistDiff,expandedViewWidth;

	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,EXPANDED_BG_X), "%" G_GINT64_FORMAT, &expandedBgx);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,FIRST_DIVIDER_LINE_X), "%" G_GINT64_FORMAT, &firstDividerLineX);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,TEXT_X_POS), "%" G_GINT64_FORMAT, &textX);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,FIRST_ICON_X), "%" G_GINT64_FORMAT, &iconX);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,TEXTURE_DIST_DIFF), "%" G_GINT64_FORMAT, &textureDistDiff);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,DIVIDER_LINE_DIST_DIFF), "%" G_GINT64_FORMAT, &dividerLineDistDiff);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,EXPANDED_VIEW_WIDTH), "%" G_GINT64_FORMAT, &expandedViewWidth);

	GList *textureList,*dividerList;
	textureList = statusBar->priv->textureList;
	dividerList = statusBar->priv->dividerLineList;
	for(loopVariable=statusBar->priv->onClicked ; loopVariable > statusBar->priv->expandedTexture ; loopVariable--)
	{

		textureList = g_list_nth(statusBar->priv->textureList,loopVariable-1);
		dividerList = g_list_nth(statusBar->priv->dividerLineList,loopVariable-2);

		clutter_actor_save_easing_state(CLUTTER_ACTOR(textureList->data));
		clutter_actor_set_easing_mode (CLUTTER_ACTOR(textureList->data), CLUTTER_EASE_IN_OUT_CUBIC);
		clutter_actor_set_easing_duration (CLUTTER_ACTOR(textureList->data), 500);
		clutter_actor_set_x (CLUTTER_ACTOR(textureList->data), (iconX+(textureDistDiff*(loopVariable-1)))*1.0);
		clutter_actor_restore_easing_state (CLUTTER_ACTOR(textureList->data));

		clutter_actor_save_easing_state(CLUTTER_ACTOR(dividerList->data));
		clutter_actor_set_easing_mode (CLUTTER_ACTOR(dividerList->data), CLUTTER_EASE_IN_OUT_CUBIC);
		clutter_actor_set_easing_duration (CLUTTER_ACTOR(dividerList->data), 500);
		clutter_actor_set_x (CLUTTER_ACTOR(dividerList->data),(firstDividerLineX+(dividerLineDistDiff*(loopVariable-2)))*1.0  );
		clutter_actor_set_y (CLUTTER_ACTOR(dividerList->data), 5.0  );
		clutter_actor_restore_easing_state (CLUTTER_ACTOR(dividerList->data));

		clutter_actor_save_easing_state(CLUTTER_ACTOR(statusBar->priv->darkImage));
		clutter_actor_set_easing_mode (CLUTTER_ACTOR(statusBar->priv->darkImage), CLUTTER_EASE_IN_OUT_CUBIC);
		clutter_actor_set_easing_duration (CLUTTER_ACTOR(statusBar->priv->darkImage), 500);
		clutter_actor_set_x (CLUTTER_ACTOR(statusBar->priv->darkImage),(expandedBgx+(textureDistDiff*(statusBar->priv->onClicked-1)))*1.0);
		clutter_actor_set_y (CLUTTER_ACTOR(statusBar->priv->darkImage), 1.0  );
		clutter_actor_restore_easing_state (CLUTTER_ACTOR(statusBar->priv->darkImage));

	}
}

static void animate_right_side(gpointer userData)
{
	StatusBar *statusBar = STATUS_BAR(userData);
	int loopVariable=0;

	gint64 expandedBgx;
	gint64 firstDividerLineX;
	gint64 textX;
	gint64 iconX;
	gint64 textureDistDiff,dividerLineDistDiff,expandedViewWidth;

	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,EXPANDED_BG_X), "%" G_GINT64_FORMAT, &expandedBgx);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,FIRST_DIVIDER_LINE_X), "%" G_GINT64_FORMAT, &firstDividerLineX);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,TEXT_X_POS), "%" G_GINT64_FORMAT, &textX);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,FIRST_ICON_X), "%" G_GINT64_FORMAT, &iconX);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,TEXTURE_DIST_DIFF), "%" G_GINT64_FORMAT, &textureDistDiff);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,DIVIDER_LINE_DIST_DIFF), "%" G_GINT64_FORMAT, &dividerLineDistDiff);
	sscanf((gchar*) g_hash_table_lookup(statusBar->priv->pLocalHash,EXPANDED_VIEW_WIDTH), "%" G_GINT64_FORMAT, &expandedViewWidth);

	GList *textureList,*dividerList;
	textureList = statusBar->priv->textureList;
	dividerList = statusBar->priv->dividerLineList;
	for(loopVariable=statusBar->priv->onClicked ; loopVariable < statusBar->priv->expandedTexture ; loopVariable++)
	{
		textureList = g_list_nth(statusBar->priv->textureList,loopVariable);
		dividerList = g_list_nth(statusBar->priv->dividerLineList,loopVariable-1);

		clutter_actor_save_easing_state(CLUTTER_ACTOR(textureList->data));
		clutter_actor_set_easing_mode (CLUTTER_ACTOR(textureList->data), CLUTTER_EASE_IN_OUT_CUBIC);
		clutter_actor_set_easing_duration (CLUTTER_ACTOR(textureList->data), 500);
		clutter_actor_set_x (CLUTTER_ACTOR(textureList->data), ((iconX+(textureDistDiff*(loopVariable)))+expandedViewWidth)*1.0  );
		clutter_actor_restore_easing_state (CLUTTER_ACTOR(textureList->data));

		clutter_actor_save_easing_state(CLUTTER_ACTOR(dividerList->data));
		clutter_actor_set_easing_mode (CLUTTER_ACTOR(dividerList->data), CLUTTER_EASE_IN_OUT_CUBIC);
		clutter_actor_set_easing_duration (CLUTTER_ACTOR(dividerList->data), 500);
		clutter_actor_set_x (CLUTTER_ACTOR(dividerList->data),((firstDividerLineX+(dividerLineDistDiff*(loopVariable-1)))+expandedViewWidth)*1.0  );
		clutter_actor_set_y (CLUTTER_ACTOR(dividerList->data), 5.0  );
		clutter_actor_restore_easing_state (CLUTTER_ACTOR(dividerList->data));

		clutter_actor_save_easing_state(CLUTTER_ACTOR(statusBar->priv->darkImage));
		clutter_actor_set_easing_mode (CLUTTER_ACTOR(statusBar->priv->darkImage), CLUTTER_EASE_IN_OUT_CUBIC);
		clutter_actor_set_easing_duration (CLUTTER_ACTOR(statusBar->priv->darkImage), 500);
		clutter_actor_set_x (CLUTTER_ACTOR(statusBar->priv->darkImage),(expandedBgx+(textureDistDiff*(statusBar->priv->onClicked-1)))*1.0 );
		clutter_actor_set_y (CLUTTER_ACTOR(statusBar->priv->darkImage), 1.0  );
		clutter_actor_restore_easing_state (CLUTTER_ACTOR(statusBar->priv->darkImage));
	}
}

/********************************************************
 * Function : update_status_bar
 * Description: Update the information of the statusbar.(Ex.Animation,text update).
 * Parameter :  *object,*name
 * Return value: void
 ********************************************************/
void update_status_bar(GObject *object,gchar *name)
{
	StatusBar *statusBar = STATUS_BAR(object);

	if(statusBar->priv->onClicked > statusBar->priv->expandedTexture)
	{
		animate_left_side(object);
	}

	if(statusBar->priv->onClicked < statusBar->priv->expandedTexture)
	{
		animate_right_side(object);
	}
}

/********************************************************
 * Function : on_button_press
 * Description: Call back function for the button press signal.
 * Parameter :  *actor,*event,userData
 * Return value: gboolean
 ********************************************************/
static gboolean on_button_press(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	return TRUE;
}

/********************************************************
 * Function : update_status_bar_clock
 * Description: Updates the time of the statusbar.
 * Parameter :  data
 * Return value: gboolean
 ********************************************************/
static gboolean update_status_bar_clock (gpointer data)
{
	ClutterActor *cTime=CLUTTER_ACTOR(data);
	//status_bar_debug("Called");
	struct tm *t;
	time_t *ts = (time_t *) malloc (sizeof (time_t));
	*ts = time (NULL);
	t = localtime (ts);
	gchar *time = g_strdup_printf ("%02d:%02d", t->tm_hour, t->tm_min);
	//status_bar_debug("Time %s \n", time );
	free (ts);
	//Set the current Time.
	clutter_text_set_text (CLUTTER_TEXT(cTime), time);
	g_free ((gpointer)time);
	//status_bar_debug("Finished");
	return TRUE;
}

static void status_bar_class_init (StatusBarClass *klass)
{

	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	g_type_class_add_private (klass, sizeof (StatusBarPrivate));
	object_class->get_property = status_bar_get_property;
	object_class->set_property = status_bar_set_property;
	object_class->dispose = status_bar_dispose;
	object_class->finalize = status_bar_finalize;
	//status_bar_debug("\n Class Init \n");
}

static void status_bar_init (StatusBar *self)
{
	GError *error = NULL;
	g_autoptr (CbyComponentIndex) component_index = NULL;

	self->priv = BAR_PRIVATE (self);
	//status_bar_debug("\n Init \n");


	gchar *temp = g_get_current_dir();
	self->priv->PKG_DATA_DIRECTORY = g_strconcat(temp,"/share",NULL);
	if(temp)
	{
		g_free(temp);
		temp=NULL;
	}
	//g_print("\n Working Dir path = %s",self->priv->PKG_DATA_DIRECTORY);
	gchar *pJsonPath = g_strconcat(self->priv->PKG_DATA_DIRECTORY,"/mildenhall_status_bar_style.json",NULL);
	self->priv->pStyleHash = thornbury_style_set(pJsonPath);
	if(NULL != self->priv->pStyleHash)
	{
		GHashTableIter iter;
		gpointer key, value;
		g_hash_table_iter_init(&iter, self->priv->pStyleHash);
		/* iter per layer */
		while (g_hash_table_iter_next (&iter, &key, &value))
		{
			GHashTable *pHash = value;
			if(NULL != pHash)
			{
				self->priv->pLocalHash = g_hash_table_new_full(g_int_hash, g_str_equal, NULL, g_free);
				g_hash_table_foreach(pHash, v_status_bar_parse_style, self);
			}
		}
	}
	thornbury_style_free(self->priv->pStyleHash);
	status_bar_free_pkgdatadir_text(pJsonPath);

	/* Create entry point index */
	component_index = cby_component_index_new (CBY_COMPONENT_INDEX_FLAGS_NONE, &error);
	if (error)
	{
		g_warning ("%s: code %d: %s", g_quark_to_string (error->domain),
		           error->code, error->message);
		g_error_free (error);
		self->priv->entry_point_index = NULL;
	}
	else
	{
		self->priv->entry_point_index = cby_entry_point_index_new (component_index);
		g_debug ("entry point index created");
	}
}

/********************************************************
 * Function : v_status_bar_parse_style
 * Description: Parse the status bar style json file.
 * Parameter :  pKey,pValue,pUserData
 * Return value: void
 ********************************************************/
static void v_status_bar_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	gchar *pStyleKey = g_strdup(pKey);
	StatusBar *statusBar=STATUS_BAR(pUserData);

	// store the style images in a hash for setting syle based on set properties

	if(g_strcmp0(pStyleKey, "default-text") == 0)
		v_status_bar_add_style_to_hash(statusBar, DEFAULT_TEXT, pValue);
	else if(g_strcmp0(pStyleKey, "default-icon") == 0)
		v_status_bar_add_style_to_hash(statusBar, DEFAULT_ICON, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-font") == 0)
		v_status_bar_add_style_to_hash(statusBar, STATUS_BAR_FONT, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-bg") == 0)
		v_status_bar_add_style_to_hash(statusBar, STATUS_BAR_BG, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-container-bg") == 0)
		v_status_bar_add_style_to_hash(statusBar, STATUS_BAR_CONTAINER_BG, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-container-width") == 0)
		v_status_bar_add_style_to_hash(statusBar, STATUS_BAR_CONTAINER_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-container-height") == 0)
		v_status_bar_add_style_to_hash(statusBar, STATUS_BAR_CONTAINER_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-container-pos-x") == 0)
		v_status_bar_add_style_to_hash(statusBar, STATUS_BAR_CONTAINER_X, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-container-pos-y") == 0)
		v_status_bar_add_style_to_hash(statusBar, STATUS_BAR_CONTAINER_Y, pValue);
	else if(g_strcmp0(pStyleKey, "expanded-bg-x") == 0)
		v_status_bar_add_style_to_hash(statusBar, EXPANDED_BG_X, pValue);
	else if(g_strcmp0(pStyleKey, "expanded-bg-y") == 0)
		v_status_bar_add_style_to_hash(statusBar, EXPANDED_BG_Y, pValue);
	else if(g_strcmp0(pStyleKey, "expanded-bg-width") == 0)
		v_status_bar_add_style_to_hash(statusBar, EXPANDED_BG_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "expanded-bg-height") == 0)
		v_status_bar_add_style_to_hash(statusBar, EXPANDED_BG_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "exapanded-background") == 0)
		v_status_bar_add_style_to_hash(statusBar, EXPANDED_BG_IMG, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-left-arrow") == 0)
		v_status_bar_add_style_to_hash(statusBar, LEFT_ARROW_IMG, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-right-arrow") == 0)
		v_status_bar_add_style_to_hash(statusBar, RIGHT_ARROW_IMG, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-left-arrow-x") == 0)
		v_status_bar_add_style_to_hash(statusBar, LEFT_ARROW_X, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-right-arrow-x") == 0)
		v_status_bar_add_style_to_hash(statusBar, RIGHT_ARROW_X, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-arrow-y") == 0)
		v_status_bar_add_style_to_hash(statusBar, STATUS_BAR_ARROW_Y, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-width") == 0)
		v_status_bar_add_style_to_hash(statusBar, STATUS_BAR_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "status-bar-height") == 0)
		v_status_bar_add_style_to_hash(statusBar, STATUS_BAR_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "divider-line-width") == 0)
		v_status_bar_add_style_to_hash(statusBar, DIVIDER_LINE_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "divider-line-height") == 0)
		v_status_bar_add_style_to_hash(statusBar, DIVIDER_LINE_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "left-divider-line-x") == 0)
		v_status_bar_add_style_to_hash(statusBar, LEFT_DIVIDER_LINE_X, pValue);
	else if(g_strcmp0(pStyleKey, "right-divider-line-x") == 0)
		v_status_bar_add_style_to_hash(statusBar, RIGHT_DIVIDER_LINE_X, pValue);
	else if(g_strcmp0(pStyleKey, "first-divider-line-x") == 0)
		v_status_bar_add_style_to_hash(statusBar, FIRST_DIVIDER_LINE_X, pValue);
	else if(g_strcmp0(pStyleKey, "divider-line-y") == 0)
		v_status_bar_add_style_to_hash(statusBar, DIVIDER_LINE_Y, pValue);
	else if(g_strcmp0(pStyleKey, "expanded-view-width") == 0)
		v_status_bar_add_style_to_hash(statusBar, EXPANDED_VIEW_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "time-x-pos") == 0)
		v_status_bar_add_style_to_hash(statusBar, TIME_X_POS, pValue);
	else if(g_strcmp0(pStyleKey, "time-y-pos") == 0)
		v_status_bar_add_style_to_hash(statusBar, TIME_Y_POS, pValue);
	else if(g_strcmp0(pStyleKey, "text-x-pos") == 0)
		v_status_bar_add_style_to_hash(statusBar, TEXT_X_POS, pValue);
	else if(g_strcmp0(pStyleKey, "text-y-pos") == 0)
		v_status_bar_add_style_to_hash(statusBar, TEXT_Y_POS, pValue);
	else if(g_strcmp0(pStyleKey, "text-width") == 0)
		v_status_bar_add_style_to_hash(statusBar, TEXT_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "text-height") == 0)
		v_status_bar_add_style_to_hash(statusBar, TEXT_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "icon-width") == 0)
		v_status_bar_add_style_to_hash(statusBar, ICON_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "icon-height") == 0)
		v_status_bar_add_style_to_hash(statusBar, ICON_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "first-icon-x") == 0)
		v_status_bar_add_style_to_hash(statusBar, FIRST_ICON_X, pValue);
	else if(g_strcmp0(pStyleKey, "first-icon-y") == 0)
		v_status_bar_add_style_to_hash(statusBar, FIRST_ICON_Y, pValue);
	else if(g_strcmp0(pStyleKey, "texture-dist-diff") == 0)
		v_status_bar_add_style_to_hash(statusBar, TEXTURE_DIST_DIFF, pValue);
	else if(g_strcmp0(pStyleKey, "divider-line-dist-diff") == 0)
		v_status_bar_add_style_to_hash(statusBar, DIVIDER_LINE_DIST_DIFF, pValue);
	else
		;	// do nothing
}

/********************************************************
 * Function : v_status_bar_add_style_to_hash
 * Description: maintain style hash
 * Parameter :  *statusBar, *pKey, pValue
 * Return value: void
 ********************************************************/
static void v_status_bar_add_style_to_hash(StatusBar *statusBar, gchar *pKey, gpointer pValue)
{
	if(!STATUS_IS_BAR(statusBar))
	{
		g_warning("invalid bottom bar object\n");
		return;
	}
	StatusBarPrivate *priv = statusBar->priv;
	if(NULL != pKey || NULL != pValue)
	{
		if (G_VALUE_HOLDS_INT64(pValue))
			g_hash_table_insert(priv->pLocalHash, pKey,g_strdup_printf("%" G_GINT64_FORMAT, g_value_get_int64(pValue)));
		else
			g_hash_table_insert(priv->pLocalHash, pKey, g_value_dup_string(pValue));
	}
}
