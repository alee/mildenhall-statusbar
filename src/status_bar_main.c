/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* status_bar_main.c
 *
 * Created on: Dec 18,2012
 * Author: Thushara.M.S
 *
 * status_bar_main.c */

#include <clutter/gdk/clutter-gdk.h>
#include <mildenhall/mildenhall.h>

#include "status_bar.h"
#include "canterbury_app_handler.h"
ClutterActor      *status_bar_stage = NULL;
MildenhallStatusbar *status_bar_object = NULL;
gboolean status_bar_set_doc_type(gpointer data);

static void v_mildelhall_launch_mgr_proxy_clb( GObject *pSourceObject,GAsyncResult *pResult,gpointer pUserData);
static void on_app_mgr_name_appeared (GDBusConnection *connection,const gchar *pName,const gchar *pNameOwner,gpointer pUserData);
static void on_app_mgr_name_vanished(GDBusConnection *connection,const gchar *name,gpointer pUserData);
static void status_bar_appmgr_proxy_clb( GObject *source_object,GAsyncResult *res,gpointer pUserData);
static gboolean handle_remove_status_bar_info(MildenhallStatusbar *object,GDBusMethodInvocation *invocation,const gchar *arg_app_name,gpointer user_data);
//Signal Handler


void status_bar_free_pkgdatadir_text(gchar *msg_text)
{
	if(msg_text)
	{
		g_free(msg_text);
		msg_text=NULL;
	}
}

static void update_audio_info (StatusBar *status_bar, guint audio_type)
{
	int inNumRows;
	gboolean isApplication=FALSE;
	int rowNumber=0;
	GList *textureList = NULL;
	GList *dividerList = NULL;
	gpointer texturetemp=NULL;
	gpointer infoTemp = NULL;
	//g_print("\n AUDIO RESOURCE TYPE = %d\n",status_bar->priv->inAudioResourceType);

	status_bar->priv->inAudioResourceType = audio_type;

	if((g_strcmp0(status_bar->priv->curAudioSource,status_bar->priv->arg_app_name)))
	{
		//If current audio source and app name are not same then just add a dummy item and display the status in the second row of the status bar.
		if(!(g_list_length(status_bar->priv->appList)))
		{
			//g_print("\n Len is zero and appending \n");
			status_bar->priv->appList = g_list_append(status_bar->priv->appList,(gpointer)g_strdup("DEFAULT"));
			status_bar->priv->iconList = g_list_append(status_bar->priv->iconList,(gpointer)g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,mildenhall_STATUS_BAR_DEFAULT_APP_ICON,NULL));
			status_bar->priv->textList = g_list_append(status_bar->priv->textList,(gpointer)g_strdup(mildenhall_STATUS_BAR_DEFAULT_TEXT));
			//status_bar->priv->appList = g_list_append(status_bar->priv->appList,(gpointer)g_strdup(arg_app_name));
			dividerList=g_list_nth(status_bar->priv->dividerLineList,0);
			clutter_actor_show(dividerList->data);
		}

	}
	else
	{
		if(status_bar->priv->inAudioResourceType == CANTERBURY_AUDIO_RESOURCE_INTERRUPT || status_bar->priv->inAudioResourceType == CANTERBURY_AUDIO_RESOURCE_MUSIC || status_bar->priv->inAudioResourceType == CANTERBURY_AUDIO_RESOURCE_VIDEO)
		{
			gpointer texturetemp=g_list_nth_data(status_bar->priv->textureList,0);
			thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(texturetemp),status_bar->priv->arg_icon_path,0,0,FALSE,FALSE);
			clutter_actor_set_y(CLUTTER_ACTOR(texturetemp),4);
			gpointer infoTemp=g_list_nth_data(status_bar->priv->statusInfoList,0);
			clutter_text_set_text(CLUTTER_TEXT(infoTemp),status_bar->priv->arg_status_bar_text);
			//g_print("\n 1st Set Text = %s",status_bar->priv->arg_status_bar_text);
			if(!(g_list_length(status_bar->priv->appList)))
			{
				//g_print("\n Length is zero \n");
				status_bar->priv->appList = g_list_append(status_bar->priv->appList,(gpointer)g_strdup(status_bar->priv->arg_app_name));
				status_bar->priv->iconList = g_list_append(status_bar->priv->iconList,(gpointer)g_strdup(status_bar->priv->arg_icon_path));
				status_bar->priv->textList = g_list_append(status_bar->priv->textList,(gpointer)g_strdup(status_bar->priv->arg_status_bar_text));
			}
			else
			{
				//g_print("\n Remove and append and Len = %d",g_list_length(status_bar->priv->appList));
				status_bar->priv->appList = g_list_remove(status_bar->priv->appList,g_list_nth_data(status_bar->priv->appList,0));
				status_bar->priv->appList = g_list_insert(status_bar->priv->appList,(gpointer)g_strdup(status_bar->priv->arg_app_name),0);
				status_bar->priv->iconList = g_list_remove(status_bar->priv->iconList,g_list_nth_data(status_bar->priv->iconList,0));
				status_bar->priv->iconList = g_list_insert(status_bar->priv->iconList,(gpointer)g_strdup(status_bar->priv->arg_icon_path),0);
				status_bar->priv->textList = g_list_remove(status_bar->priv->textList,g_list_nth_data(status_bar->priv->textList,0));
				status_bar->priv->textList = g_list_insert(status_bar->priv->textList,(gpointer)g_strdup(status_bar->priv->arg_status_bar_text),0);
			}
			isApplication = TRUE;
		}
	}
	for (inNumRows = 0; inNumRows < g_list_length(status_bar->priv->appList); inNumRows++)
	{
		++rowNumber;
		//g_print("\n List data = %s \n",(gchar *)g_list_nth_data(status_bar->priv->appList,inNumRows));
		if(!(g_strcmp0((gchar *)g_list_nth_data(status_bar->priv->appList,inNumRows),status_bar->priv->arg_app_name)))
		{
			//g_print("\n Application %s exists in list and replacing & LEN = %d\n",status_bar->priv->arg_app_name,g_list_length(status_bar->priv->appList));
			GList *infoList;
			isApplication = TRUE;
			textureList = g_list_nth(status_bar->priv->textureList,rowNumber-1);
			thornbury_ui_texture_set_from_file(textureList->data,status_bar->priv->arg_icon_path,0,0,FALSE,FALSE);
			clutter_actor_set_y(CLUTTER_ACTOR(texturetemp),4);
			infoList = g_list_nth(status_bar->priv->statusInfoList,rowNumber-1);
			clutter_text_set_text(CLUTTER_TEXT(infoList->data),status_bar->priv->arg_status_bar_text);
			//g_print("\n 2SET TEXT = %s",status_bar->priv->arg_status_bar_text);
			status_bar->priv->iconList = g_list_remove(status_bar->priv->iconList,g_list_nth_data(status_bar->priv->iconList,inNumRows));
			status_bar->priv->iconList = g_list_insert(status_bar->priv->iconList,(gpointer)g_strdup(status_bar->priv->arg_icon_path),inNumRows);
			status_bar->priv->textList = g_list_remove(status_bar->priv->textList,g_list_nth_data(status_bar->priv->textList,inNumRows));
			status_bar->priv->textList = g_list_insert(status_bar->priv->textList,(gpointer)g_strdup(status_bar->priv->arg_status_bar_text),inNumRows);
			break;
		}
		else
		{
			//g_print("\n Application %s doesn't exists in list and LEN = %d \n",status_bar->priv->arg_app_name,g_list_length(status_bar->priv->appList));
			isApplication = FALSE;
		}
	}


	if(isApplication == FALSE && g_list_length(status_bar->priv->appList) < 6)
	{
		//g_print("\n Before switch Len = %d",g_list_length(status_bar->priv->appList));
		switch(g_list_length(status_bar->priv->appList))
		{
		case 0:
			//g_print("\n CASE 0 \n");
			texturetemp=g_list_nth_data(status_bar->priv->textureList,0);
			thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(texturetemp),(gchar *)status_bar->priv->arg_icon_path,0,0,FALSE,FALSE);
			clutter_actor_set_y(CLUTTER_ACTOR(texturetemp),6);
			infoTemp=g_list_nth_data(status_bar->priv->statusInfoList,0);
			clutter_text_set_text(CLUTTER_TEXT(infoTemp),status_bar->priv->arg_status_bar_text);
			clutter_actor_set_reactive(status_bar->priv->textureOne,FALSE);
			dividerList=g_list_nth(status_bar->priv->dividerLineList,0);
			clutter_actor_show(dividerList->data);
			clutter_actor_hide(status_bar->priv->rightArrowTexture);
			clutter_actor_hide(status_bar->priv->leftArrowTexture);
			break;
		case 1:
			//g_print("\n CASE 1 \n");
			texturetemp=g_list_nth_data(status_bar->priv->textureList,1);
			thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(texturetemp),(gchar *)status_bar->priv->arg_icon_path,0,0,FALSE,FALSE);
			clutter_actor_set_y(CLUTTER_ACTOR(texturetemp),6);
			dividerList=g_list_nth(status_bar->priv->dividerLineList,1);
			clutter_actor_show(dividerList->data);
			clutter_actor_show(CLUTTER_ACTOR(texturetemp));
			texturetemp=g_list_nth_data(status_bar->priv->statusInfoList,1);
			clutter_text_set_text(CLUTTER_TEXT(texturetemp),status_bar->priv->arg_status_bar_text);
			if(status_bar->priv->expandedTexture == 1)
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_hide(status_bar->priv->leftArrowTexture);
			}
			else
			{
				clutter_actor_hide(status_bar->priv->rightArrowTexture);
				clutter_actor_show(status_bar->priv->leftArrowTexture);
			}
			break;
		case 2:
			//g_print("\n CASE 2 \n");
			texturetemp=g_list_nth_data(status_bar->priv->textureList,2);
			thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(texturetemp),(gchar *)status_bar->priv->arg_icon_path,0,0,FALSE,FALSE);
			clutter_actor_set_y(CLUTTER_ACTOR(texturetemp),6);
			dividerList=g_list_nth(status_bar->priv->dividerLineList,2);
			clutter_actor_show(dividerList->data);
			clutter_actor_show(CLUTTER_ACTOR(texturetemp));
			clutter_actor_set_reactive(status_bar->priv->textureOne,TRUE);
			texturetemp=g_list_nth_data(status_bar->priv->statusInfoList,2);
			clutter_text_set_text(CLUTTER_TEXT(texturetemp),status_bar->priv->arg_status_bar_text);
			if(status_bar->priv->expandedTexture != 1 && status_bar->priv->expandedTexture != g_list_length(status_bar->priv->appList))
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_show(status_bar->priv->leftArrowTexture);
			}
			if(status_bar->priv->expandedTexture == 1)
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_hide(status_bar->priv->leftArrowTexture);
			}
			if(status_bar->priv->expandedTexture == g_list_length(status_bar->priv->appList))
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_show(status_bar->priv->leftArrowTexture);
			}
			break;
		case 3:
			//g_print("\n CASE 3 \n");
			texturetemp=g_list_nth_data(status_bar->priv->textureList,3);
			thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(texturetemp),(gchar *)status_bar->priv->arg_icon_path,0,0,FALSE,FALSE);
			clutter_actor_set_y(CLUTTER_ACTOR(texturetemp),6);
			dividerList=g_list_nth(status_bar->priv->dividerLineList,3);
			clutter_actor_show(dividerList->data);
			clutter_actor_show(CLUTTER_ACTOR(texturetemp));
			texturetemp=g_list_nth_data(status_bar->priv->statusInfoList,3);
			clutter_text_set_text(CLUTTER_TEXT(texturetemp),status_bar->priv->arg_status_bar_text);
			if(status_bar->priv->expandedTexture != 1 && status_bar->priv->expandedTexture != g_list_length(status_bar->priv->appList))
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_show(status_bar->priv->leftArrowTexture);
			}
			if(status_bar->priv->expandedTexture == 1)
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_hide(status_bar->priv->leftArrowTexture);
			}
			if(status_bar->priv->expandedTexture == g_list_length(status_bar->priv->appList))
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_show(status_bar->priv->leftArrowTexture);
			}


			break;
		case 4:
			//g_print("\n CASE 4 \n");
			texturetemp=g_list_nth_data(status_bar->priv->textureList,4);
			thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(texturetemp),(gchar *)status_bar->priv->arg_icon_path,0,0,FALSE,FALSE);
			clutter_actor_set_y(CLUTTER_ACTOR(texturetemp),6);
			dividerList=g_list_nth(status_bar->priv->dividerLineList,4);
			clutter_actor_show(dividerList->data);
			clutter_actor_show(CLUTTER_ACTOR(texturetemp));
			texturetemp=g_list_nth_data(status_bar->priv->statusInfoList,4);
			clutter_text_set_text(CLUTTER_TEXT(texturetemp),status_bar->priv->arg_status_bar_text);
			if(status_bar->priv->expandedTexture != 1 && status_bar->priv->expandedTexture != g_list_length(status_bar->priv->appList))
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_show(status_bar->priv->leftArrowTexture);
			}
			if(status_bar->priv->expandedTexture == 1)
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_hide(status_bar->priv->leftArrowTexture);
			}
			if(status_bar->priv->expandedTexture == g_list_length(status_bar->priv->appList))
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_show(status_bar->priv->leftArrowTexture);
			}
			break;
		case 5:
			//g_print("\n CASE 5 \n");
			texturetemp=g_list_nth_data(status_bar->priv->textureList,5);
			thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(texturetemp),(gchar *)status_bar->priv->arg_icon_path,0,0,FALSE,FALSE);
			clutter_actor_set_y(CLUTTER_ACTOR(texturetemp),6);
			dividerList=g_list_nth(status_bar->priv->dividerLineList,5);
			clutter_actor_show(dividerList->data);
			clutter_actor_show(CLUTTER_ACTOR(texturetemp));
			texturetemp=g_list_nth_data(status_bar->priv->statusInfoList,5);
			clutter_text_set_text(CLUTTER_TEXT(texturetemp),status_bar->priv->arg_status_bar_text);
			if(status_bar->priv->expandedTexture == g_list_length(status_bar->priv->appList))
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_show(status_bar->priv->leftArrowTexture);
			}
			else if(status_bar->priv->expandedTexture != 1 && status_bar->priv->expandedTexture < g_list_length(status_bar->priv->appList))
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_show(status_bar->priv->leftArrowTexture);
			}
			if(status_bar->priv->expandedTexture == 1)
			{
				clutter_actor_show(status_bar->priv->rightArrowTexture);
				clutter_actor_hide(status_bar->priv->leftArrowTexture);
			}
			break;
		default:
			;
			break;
		}
		//g_print("\n Normal appending \n");
		status_bar->priv->appList = g_list_append(status_bar->priv->appList,(gpointer)g_strdup(status_bar->priv->arg_app_name));
		status_bar->priv->iconList = g_list_append(status_bar->priv->iconList,(gpointer)g_strdup(status_bar->priv->arg_icon_path));
		status_bar->priv->textList = g_list_append(status_bar->priv->textList,(gpointer)g_strdup(status_bar->priv->arg_status_bar_text));
	}
	int ind;
	for(ind = 0; ind < g_list_length(status_bar->priv->iconList);ind++)
	{
		//g_print("\n ICON lIST = %s \n",(gchar *)g_list_nth_data(status_bar->priv->iconList,ind));
		//g_print("\n ************************************* \n");
	}


	if(status_bar->priv->inAudioResourceType > CANTERBURY_AUDIO_RESOURCE_EXTERNAL)
	{
		gpointer texturetemp=g_list_nth_data(status_bar->priv->textureList,0);
		thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(texturetemp),g_strconcat(status_bar->priv->PKG_DATA_DIRECTORY,mildenhall_STATUS_BAR_DEFAULT_APP_ICON,NULL),0,0,FALSE,FALSE);
		clutter_actor_set_position(CLUTTER_ACTOR(texturetemp),45,-4);
		gpointer infoTemp=g_list_nth_data(status_bar->priv->statusInfoList,0);
		clutter_text_set_text(CLUTTER_TEXT(infoTemp),mildenhall_STATUS_BAR_DEFAULT_TEXT);
		//g_print("\n Audio source is none \n");
	}
}

static guint
get_audio_resource_type (CbyEntryPointIndex *entry_point_index, const gchar *id)
{
  g_autofree gchar *apertis_type = NULL;
  g_autoptr(CbyEntryPoint) entry_point = NULL;
  GEnumClass *cls;
  GEnumValue *value;

  if (entry_point_index == NULL)
     return CANTERBURY_AUDIO_RESOURCE_TYPE_UNKNOWN;

  entry_point = cby_entry_point_index_get_by_id (entry_point_index, id);
  if (entry_point == NULL)
     return CANTERBURY_AUDIO_RESOURCE_TYPE_UNKNOWN;

  apertis_type = cby_entry_point_get_string (entry_point, "X-Apertis-AudioRole");
  if (apertis_type == NULL)
     return CANTERBURY_AUDIO_RESOURCE_TYPE_UNKNOWN;

  cls = g_type_class_ref (CANTERBURY_TYPE_EXECUTABLE_TYPE);
  value = g_enum_get_value_by_nick (cls, apertis_type);
  g_type_class_unref (cls);
  if (value == NULL)
     return CANTERBURY_AUDIO_RESOURCE_TYPE_UNKNOWN;

  return value->value;
}

static gboolean handle_set_status_bar_request(MildenhallStatusbar *object,
		GDBusMethodInvocation *invocation,
		const gchar *arg_app_name,
		const gchar *arg_sub_app_name,
		const gchar *arg_icon_path,
		const gchar *arg_status_bar_text,
		gpointer user_data)
{
	g_autoptr (CbyEntryPoint) entry_point = NULL;

	//g_print("\n ************************************************* \n");
	g_print("\n StatusBar = handle_set_status_bar arg_app_name = %s and sub arg name = %s\n",arg_app_name,arg_sub_app_name);
	if(!STATUS_IS_BAR(user_data))
		return TRUE;

	StatusBar *status_bar =STATUS_BAR(user_data);

	status_bar->priv->arg_app_name = g_strdup(arg_app_name);
	status_bar->priv->arg_sub_app_name = g_strdup(arg_sub_app_name);
	status_bar->priv->arg_status_bar_text = g_strdup(arg_status_bar_text);
	status_bar->priv->arg_icon_path = g_strdup(arg_icon_path);

	//g_print("\n3344536sub arg name = %s\n",arg_sub_app_name);

	update_audio_info (status_bar, get_audio_resource_type (
                                           status_bar->priv->entry_point_index,
                                           arg_app_name));

	mildenhall_statusbar_complete_set_status_bar_info(object , invocation );
	return TRUE;
}

static void on_bus_acquired (GDBusConnection *connection,const gchar *name,gpointer user_data)
{
	status_bar_debug("on_bus_acquired %s \n" , name);

	/* here we export all the methods supported by the service, this callback is called before on_name_acquired*/
	/* once on_name_acquired is called clients can start calling service interfaces, so its important to export all methods here */

	/* Creates a new skeleton object, ready to be exported */
	status_bar_object = mildenhall_statusbar_skeleton_new ();
	/* provide signal handlers for all methods */
	g_signal_connect (status_bar_object,"handle-set-status-bar-info",G_CALLBACK (handle_set_status_bar_request),user_data);
	g_signal_connect (status_bar_object,"handle-remove-status-bar-info",G_CALLBACK (handle_remove_status_bar_info),user_data);
	/* Exports interface launcher_object at object_path "/Sample/Service" on connection */
	if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (status_bar_object),connection,"/org/apertis/Mildenhall/Statusbar",NULL))
	{
		status_bar_debug("export error \n");
	}

}

static gboolean handle_remove_status_bar_info(MildenhallStatusbar *object,
		GDBusMethodInvocation *invocation,
		const gchar *arg_app_name,
		gpointer user_data)
{
	//g_print("\n handle_remove_status_bar_info arg_app_name = %s \n",arg_app_name);
	gint pos;
	if(!STATUS_IS_BAR(user_data))
		return TRUE;
	StatusBar *status_bar =STATUS_BAR(user_data);
	if(arg_app_name == NULL)
		return TRUE;
	int inNumRows = 0;
	gboolean isAppInList = FALSE;
	for (inNumRows = 0; inNumRows < g_list_length(status_bar->priv->appList); inNumRows++)
	{
		//g_print("\n Remove List data = %s \n",(gchar *)g_list_nth_data(status_bar->priv->appList,inNumRows));
		if(!(g_strcmp0((gchar *)g_list_nth_data(status_bar->priv->appList,inNumRows),arg_app_name)))
		{
			isAppInList = TRUE;
			break;
		}
		else
		{
			isAppInList = FALSE;
		}
	}

	if(!(isAppInList))
		return FALSE;

	gint appPosition = 0;
	gint indexLoop = 0;
	for(indexLoop = 0 ; indexLoop < g_list_length(status_bar->priv->appList) ; indexLoop++)
	{
		if(!(g_strcmp0(arg_app_name,(gchar *)g_list_nth_data(status_bar->priv->appList,indexLoop))))
		{
			appPosition = (indexLoop+1);
			//g_print("\n AppPosition = %d",appPosition);
			break;
		}
	}
	pos = appPosition;
	if( appPosition == status_bar->priv->expandedTexture)
	{
		if(appPosition == g_list_length(status_bar->priv->appList))
		{
			//Just hide the actors.
			//g_print("\n Last one to remove \n");
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->textureList,(appPosition-1))));
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->dividerLineList,(appPosition-1))));
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->statusInfoList,(appPosition-1))));
			//g_print("\n Before deleting Len = %d",g_list_length(status_bar->priv->appList));
			status_bar->priv->appList = g_list_remove(status_bar->priv->appList,g_list_nth_data(status_bar->priv->appList,(appPosition-1)));
			status_bar->priv->iconList = g_list_remove(status_bar->priv->iconList,g_list_nth_data(status_bar->priv->iconList,(appPosition-1)));
			status_bar->priv->textList = g_list_remove(status_bar->priv->textList,g_list_nth_data(status_bar->priv->textList,(appPosition-1)));
			status_bar->priv->onClicked = 1;
			update_status_bar((GObject *)status_bar,"Default");
			status_bar->priv->expandedTexture = 1;
			clutter_actor_show(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->statusInfoList,0)));
			//g_print("\n After deleting Len = %d",g_list_length(status_bar->priv->appList));
		}
		else if(appPosition < g_list_length(status_bar->priv->appList))
		{

			//g_print("\n Middle one to remove = %d \n",appPosition);
			for(;appPosition < g_list_length(status_bar->priv->appList);appPosition++)
			{
				//hai
				gpointer texturetemp=g_list_nth_data(status_bar->priv->textureList,(appPosition-1));
				thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(texturetemp),(gchar *)g_list_nth_data(status_bar->priv->iconList,appPosition),0,0,FALSE,FALSE);
				clutter_text_set_text(CLUTTER_TEXT(g_list_nth_data(status_bar->priv->statusInfoList,(appPosition-1))),(gchar *)g_list_nth_data(status_bar->priv->textList,appPosition));
			}
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->textureList,(appPosition-1))));
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->dividerLineList,(appPosition-1))));
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->statusInfoList,(appPosition-1))));
			status_bar->priv->appList = g_list_remove(status_bar->priv->appList,g_list_nth_data(status_bar->priv->appList,(pos-1)));
			status_bar->priv->iconList = g_list_remove(status_bar->priv->iconList,g_list_nth_data(status_bar->priv->iconList,(pos-1)));
			status_bar->priv->textList = g_list_remove(status_bar->priv->textList,g_list_nth_data(status_bar->priv->textList,(pos-1)));

			status_bar->priv->onClicked = 1;
			update_status_bar((GObject *)status_bar,"Default");
			status_bar->priv->expandedTexture = 1;
			hide_or_show_actors(user_data);
			//clutter_actor_show(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->statusInfoList,0)));
		}
		clutter_actor_hide(status_bar->priv->leftArrowTexture);
	}
	else
	{
		//for(appPosition;appPositon <)
		if(appPosition == g_list_length(status_bar->priv->appList))
		{
			//Just hide the actors.
			//g_print("\n Last one to remove \n");
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->textureList,(appPosition-1))));
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->dividerLineList,(appPosition-1))));
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->statusInfoList,(appPosition-1))));
			//g_print("\n Before deleting Len = %d",g_list_length(status_bar->priv->appList));
			status_bar->priv->appList = g_list_remove(status_bar->priv->appList,g_list_nth_data(status_bar->priv->appList,(appPosition-1)));
			status_bar->priv->iconList = g_list_remove(status_bar->priv->iconList,g_list_nth_data(status_bar->priv->iconList,(appPosition-1)));
			status_bar->priv->textList = g_list_remove(status_bar->priv->textList,g_list_nth_data(status_bar->priv->textList,(appPosition-1)));
//			g_print("\n After deleting Len = %d",g_list_length(status_bar->priv->appList));
		}
		else if(appPosition < g_list_length(status_bar->priv->appList))
		{
			//Loop from appPosition to appList length and update the actors and hide the last actor.
			//g_print("\n Middle one to remove = %d \n",appPosition);
			for(;appPosition < g_list_length(status_bar->priv->appList);appPosition++)
			{
				//hai
				gpointer texturetemp=g_list_nth_data(status_bar->priv->textureList,(appPosition-1));
				thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(texturetemp),(gchar *)g_list_nth_data(status_bar->priv->iconList,appPosition),0,0,FALSE,FALSE);
				clutter_text_set_text(CLUTTER_TEXT(g_list_nth_data(status_bar->priv->statusInfoList,(appPosition-1))),(gchar *)g_list_nth_data(status_bar->priv->textList,appPosition));
			}
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->textureList,(appPosition-1))));
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->dividerLineList,(appPosition-1))));
			clutter_actor_hide(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->statusInfoList,(appPosition-1))));
			status_bar->priv->appList = g_list_remove(status_bar->priv->appList,g_list_nth_data(status_bar->priv->appList,(pos-1)));
			status_bar->priv->iconList = g_list_remove(status_bar->priv->iconList,g_list_nth_data(status_bar->priv->iconList,(pos-1)));
			status_bar->priv->textList = g_list_remove(status_bar->priv->textList,g_list_nth_data(status_bar->priv->textList,(pos-1)));

			if(status_bar->priv->expandedTexture > pos)
			{
				status_bar->priv->onClicked = pos;
				update_status_bar((GObject *)status_bar,"Default");
				status_bar->priv->expandedTexture = pos;
				clutter_actor_show(CLUTTER_ACTOR(g_list_nth_data(status_bar->priv->statusInfoList,(pos-1))));
			}

		}
		clutter_actor_hide(status_bar->priv->leftArrowTexture);
	}
	mildenhall_statusbar_complete_remove_status_bar_info(object,invocation,TRUE);//Change the state TRUE according to the result.
	return TRUE;
}

void
app_mgr_hk_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer user_data)
{
	GError *error;

	StatusBarPrivate *priv = STATUS_BAR (user_data)->priv;

	status_bar_debug("app_mgr_hk_proxy_clb \n");
	/* finishes the proxy creation and gets the proxy ptr */
	priv->app_mgr_hk_proxy = canterbury_hard_keys_proxy_new_finish(res , &error);

	if(priv->app_mgr_hk_proxy == NULL)
	{
		g_printerr("error %s \n",g_dbus_error_get_remote_error(error));
		return;
	}
}

static gboolean g_confirmation_result_clb(
		BarkwayService *object,
		const gchar *app_name,
		const gchar *arg_title,
		const gchar *arg_confirmation_result,
		gint confirmation_value,gpointer user_data)
{
	StatusBarPrivate *priv = STATUS_BAR (user_data)->priv;
	if(!g_strcmp0(app_name, priv->mildenhall_STATUS_BAR_APP_NAME))
	{
		if(!g_strcmp0(arg_confirmation_result,"YES"))
		{
			StatusBarPrivate *priv = STATUS_BAR (user_data)->priv;
			canterbury_hard_keys_call_shutdown(priv->app_mgr_hk_proxy,NULL, NULL , NULL);
		}
	}
	return TRUE;
}

static void statusbar_popup_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer user_data)
{
	StatusBarPrivate *priv = STATUS_BAR (user_data)->priv;
	GError *error=NULL;
	priv->popup_proxy=barkway_service_proxy_new_finish(res , &error);
	g_signal_connect(priv->popup_proxy,"confirmation-result",(GCallback)g_confirmation_result_clb,user_data);
}

static void
on_popup_name_appeared (GDBusConnection *connection,const gchar *name,const gchar *name_owner,gpointer user_data)
{
	barkway_service_proxy_new(connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Barkway",
			"/org/apertis/Barkway/Service",
			NULL,
			statusbar_popup_proxy_clb,
			user_data);
}

static void
on_popup_name_vanished(GDBusConnection *connection,const gchar *name,gpointer user_data)
{
	StatusBarPrivate *priv = STATUS_BAR (user_data)->priv;
	status_bar_debug("name_vanished \n");
	if(NULL != priv->popup_proxy)
		g_object_unref(priv->popup_proxy);
}

/********************************************************
 * Function : status_connection_active
 * Description: Create the statusbar from the previously stored data in the database.
 * Parameter :  *statusBar, *tableData
 * Return value: void
 ********************************************************/
static void status_connection_active (BeckfootStatus *object,const gchar *arg_network_id,const gchar *arg_network_type,gpointer userData)
{
	//StatusBarPrivate *priv = STATUS_BAR (userData)->priv;
	status_bar_debug("\n Connection Active STATUS BAR \n");
	if(!(g_strcmp0(arg_network_type,BECKFOOT_NETWORK_TYPE_WIFI)))
	{
		status_bar_debug(" Network type = %s \n", arg_network_type);
		//guchar initStrengthChanged = beckfoot_status_get_wi_fi_active_network_strength(object);
		//status_bar_debug(" initStrengthChanged = %d \n", initStrengthChanged );

		//g_print("Wifi Icon = %s \n", WIFI_SETTINGS_SIGNAL_STRENGTH(initStrengthChanged/10));
		//thornbury_ui_texture_set_from_file(priv->umtsIcon,g_strconcat(priv->PKG_DATA_DIRECTORY,WIFI_SETTINGS_SIGNAL_STRENGTH(initStrengthChanged/10),NULL),55,20,FALSE,FALSE);
	}
}

/********************************************************
 * Function : status_connection_active_lost
 * Description: Create the statusbar from the previously stored data in the database.
 * Parameter :  *statusBar, *tableData
 * Return value: void
 ********************************************************/
static void status_connection_active_lost (BeckfootStatus *object,
		const gchar *arg_network_id,
		const gchar *arg_network_type,gpointer userData)
{
	StatusBarPrivate *priv = STATUS_BAR (userData)->priv;
	status_bar_debug("\n Connection Active LOst STATUS BAR \n");
	gchar *pIconPath = g_strconcat(priv->PKG_DATA_DIRECTORY,"/icon_SignalStrength_0.png",NULL);
	thornbury_ui_texture_set_from_file(priv->umtsIcon,pIconPath,55,20,FALSE,FALSE);
	status_bar_free_pkgdatadir_text(pIconPath);
}


/********************************************************
 * Function : wifi_signal_strength_chaged
 * Description: Create the statusbar from the previously stored data in the database.
 * Parameter :  *statusBar, *tableData
 * Return value: void
 ********************************************************/
static void wifi_signal_strength_chaged (BeckfootStatus *object,const gchar *arg_network_id,guchar arg_strength,gpointer userData)
{
	StatusBarPrivate *priv = STATUS_BAR (userData)->priv;
	status_bar_debug("\n Status bar signal Strength = %d \n",arg_strength);
//	status_bar_debug("\n Icon path = %s",WIFI_SETTINGS_SIGNAL_STRENGTH(1));
	thornbury_ui_texture_set_from_file(priv->umtsIcon,g_strconcat(priv->PKG_DATA_DIRECTORY,WIFI_SETTINGS_SIGNAL_STRENGTH(arg_strength/10),NULL),55,20,FALSE,FALSE);
}

/********************************************************
 * Function : connection_manager_proxy_created_clb
 * Description: Create the statusbar from the previously stored data in the database.
 * Parameter :  *statusBar, *tableData
 * Return value: void
 ********************************************************/
static void connection_manager_proxy_created_clb( GObject *source_object,GAsyncResult *res,gpointer user_data)
{
	GError *error;
	StatusBarPrivate *priv = STATUS_BAR (user_data)->priv;
	status_bar_debug("\n Proxy Created Cb STATUS BAR \n");
	/* finishes the proxy creation and gets the proxy ptr */
	priv->Conproxy = beckfoot_status_proxy_new_finish (res , &error);
	if(priv->Conproxy == NULL)
	{
		g_print("\n Error intializing the proxy");
		exit(0);
	}
	else
	{
		gchar *nType = NULL;
		g_signal_connect(priv->Conproxy,"connection_active",(GCallback)status_connection_active,user_data);
		g_signal_connect(priv->Conproxy,"active_connection_lost",(GCallback)status_connection_active_lost,user_data);
		g_signal_connect(priv->Conproxy,"signal_strength_changed",(GCallback)wifi_signal_strength_chaged,user_data);
		nType = g_strdup(beckfoot_status_get_active_network_type (priv->Conproxy));
		if(!(g_strcmp0(nType,BECKFOOT_NETWORK_TYPE_WIFI)))
		{
			guchar initStrengthChanged = beckfoot_status_get_wi_fi_active_network_strength(priv->Conproxy);	
			thornbury_ui_texture_set_from_file(priv->umtsIcon,g_strconcat(priv->PKG_DATA_DIRECTORY,WIFI_SETTINGS_SIGNAL_STRENGTH(initStrengthChanged/10),NULL),55,20,FALSE,FALSE);
		}
		else
		{
			// Show wifi disabled icon.
			gchar *pIconPath = g_strconcat(priv->PKG_DATA_DIRECTORY,"/icon_SignalStrength_0.png",NULL);
			thornbury_ui_texture_set_from_file(priv->umtsIcon,pIconPath,55,20,FALSE,FALSE);
			status_bar_free_pkgdatadir_text(pIconPath);
			
		}
	}

}

static void name_appeared (GDBusConnection *connection,	const gchar *name,const gchar *name_owner,gpointer user_data)

{
	//g_print("beckfoot.Status Name_appeared \n");
	beckfoot_status_proxy_new (connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Beckfoot",
			"/org/apertis/Beckfoot/Status",
			NULL,
			connection_manager_proxy_created_clb,
			user_data);

}




static void name_vanished(GDBusConnection *connection,const gchar *name,gpointer user_data)
{
	StatusBarPrivate *priv = STATUS_BAR (user_data)->priv;
	//g_print("name_vanished \n");
	if(NULL != priv->Conproxy)
		g_object_unref(priv->Conproxy);
}


static void on_notify_current_audio_source ( GObject    *object,
		GParamSpec *pspec,
		gpointer    user_data)
{
	//	g_print("\n on notify currnet audio source \n");
	StatusBarPrivate *priv = STATUS_BAR (user_data)->priv;
	priv->curAudioSource = g_strdup(canterbury_app_manager_get_current_active_audio_source (priv->app_mgr_proxy));
	if(priv->curAudioSource != NULL)
	{
		//g_print("\n StatusBar AS property CHanged: Current Audio Source = %s \n ",priv->curAudioSource);

	}
}


static void on_name_acquired (GDBusConnection *connection,const gchar *name,gpointer user_data)
{
	status_bar_debug("on_name_acquired %s \n" , name);

	g_bus_watch_name (G_BUS_TYPE_SESSION,
			"org.apertis.Canterbury",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			on_app_mgr_name_appeared,
			on_app_mgr_name_vanished,
			user_data,
			NULL);


	g_bus_watch_name (G_BUS_TYPE_SESSION,
			"org.apertis.Barkway",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			on_popup_name_appeared,
			on_popup_name_vanished,
			user_data,
			NULL);

	g_bus_watch_name (G_BUS_TYPE_SESSION,
			"org.apertis.Beckfoot",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			name_appeared,
			name_vanished,
			user_data,
			NULL);


}

static void on_name_lost (GDBusConnection *connection,const gchar *name,gpointer user_data)
{
	/* free all dbus handlers ... */
	status_bar_debug("on_name_lost \n");
}

int main( gint argc, gchar **argv )
{
	int clInErr = clutter_init (&argc,&argv);
	GdkWindow *gdk_window;


	if (clInErr != CLUTTER_INIT_SUCCESS)
		return -1;
	gdk_init(&argc, &argv);
	ClutterColor status_bar_stage_color = { 0x00, 0x00, 0x00, 0xff };

	status_bar_stage = clutter_stage_new();
	clutter_actor_set_size (status_bar_stage, 800, 50);
	clutter_actor_set_background_color (CLUTTER_ACTOR (status_bar_stage), &status_bar_stage_color);
	clutter_actor_show (status_bar_stage);
	gchar *pAppName = NULL;
	if(NULL != argv)
	{
		gint i = 0;
		while (argv[i])
		{
			if(g_strcmp0(argv[i],"app-name") == 0)
			{
				g_print("\n STATUS BAR APP NAME = %s $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ \n",argv[i+1]);
				pAppName  = g_strdup(argv[i+1]);
				break;
			}
			i++;
		}
	}
	else
		g_print("\n ARGV IS NULL \n");

	ClutterActor *status_bar = NULL;
	status_bar = mildenhall_status_bar_create(pAppName);

	clutter_actor_add_child (CLUTTER_ACTOR (status_bar_stage),status_bar);

	clutter_actor_show (status_bar_stage);

	gdk_window = clutter_gdk_get_stage_window (CLUTTER_STAGE (status_bar_stage));
	gdk_window_set_type_hint (gdk_window, GDK_WINDOW_TYPE_HINT_DOCK);

	/* all services are required to own a bus before they export their interfaces */
	g_bus_own_name ( G_BUS_TYPE_SESSION,           // bus type
			"org.apertis.Mildenhall.Statusbar",      // interface name
			G_BUS_NAME_OWNER_FLAGS_NONE,  // bus own flag, can be used to take away the bus and give it to another service
			on_bus_acquired,              // callback invoked when the bus is acquired
			on_name_acquired,             // callback invoked when interface name is acquired
			on_name_lost,                 // callback invoked when name is lost to another service or other reason
			(gpointer)status_bar,                   // user data
			NULL);
	clutter_main ();
	return TRUE;
}


static void v_mildelhall_launch_mgr_proxy_clb( GObject *pSourceObject,GAsyncResult *pResult,gpointer pUserData)
{

	StatusBar *statusBar = STATUS_BAR(pUserData);
	GError *pError;

	statusBar->priv->pAppLaunchMgrProxy = canterbury_app_db_handler_proxy_new_finish(pResult , &pError);

	if(statusBar->priv->pAppLaunchMgrProxy == NULL)
	{
		g_printerr("error %s \n",g_dbus_error_get_remote_error(pError));
		return;
	}
}

/* *********************************************************************
 * Function Name : on_app_mgr_name_appeared
 * Parameters   : Connection object,pName,pNameOwner,MoneyConvert Object.
 * Description  : Name appeared callback function for App manager.
 * Return Value	: void
 *********************************************************************** */
static void
on_app_mgr_name_appeared (GDBusConnection *connection,
		const gchar     *name,
		const gchar     *name_owner,
		gpointer         pUserData)
{
	canterbury_app_manager_proxy_new(connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Canterbury",
			"/org/apertis/Canterbury/AppManager",
			NULL,
			status_bar_appmgr_proxy_clb,
			pUserData);


	/* Asynchronously creates a proxy for the App manager D-Bus interface */
	canterbury_hard_keys_proxy_new (
			connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Canterbury",
			"/org/apertis/Canterbury/HardKeys",
			NULL,
			app_mgr_hk_proxy_clb,
			pUserData);
	/* Asynchronously creates a proxy for the App manager D-Bus interface */
 		canterbury_app_db_handler_proxy_new(
			connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Canterbury",
			"/org/apertis/Canterbury/AppDbHandler",
			NULL,
			v_mildelhall_launch_mgr_proxy_clb,
			pUserData);

}

/* *********************************************************************
 * Function Name : on_app_mgr_name_vanished
 * Parameters   : Connection Object,name,MoneyConvert Object.
 * Description  : Name vanished callback function for App manager
 * Return Value	: void
 *********************************************************************** */
static void
on_app_mgr_name_vanished(GDBusConnection *connection,
		const gchar     *name,
		gpointer         pUserData)
{
	StatusBar *statusBar = STATUS_BAR(pUserData);
	if(!statusBar)
		return;
	if(NULL != statusBar->priv->app_mgr_proxy)
		g_object_unref(statusBar->priv->app_mgr_proxy);
}


/* *********************************************************************
 * Function Name : status_bar_appmgr_proxy_clb
 * Parameters   : *object,*res,MoneyConvert Object.
 * Description  : Proxy callback function for app manager.
 * Return Value	: void
 *********************************************************************** */
static void status_bar_appmgr_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer pUserData)
{
	StatusBar *statusBar = STATUS_BAR(pUserData);
	if(!statusBar)
		return;
	GError *error=NULL;
	statusBar->priv->app_mgr_proxy = canterbury_app_manager_proxy_new_finish(res , &error);
	if(statusBar->priv->app_mgr_proxy == NULL)
	{
		g_print("StatusBar App Manager :error %s \n",g_dbus_error_get_remote_error(error));
	}

	g_signal_connect (statusBar->priv->app_mgr_proxy,"notify::current-active-audio-source",
			G_CALLBACK (on_notify_current_audio_source),
			pUserData);
}


