/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* status_bar.h
 *
 * Created on: Dec 18,2012
 * Author: Thushara.M.S
 *
 * status_bar.h */

#ifndef __STATUS_BAR_H__
#define __STATUS_BAR_H__
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <clutter/clutter.h>
#include <string.h>
#include <glib.h>
#include <glib-object.h>
#include <glib/gprintf.h>
#include <glib-object.h>
#include <thornbury/thornbury.h>
#include "seaton-preference.h"
#include "canterbury.h"
#include <canterbury/canterbury-platform.h>
#include "barkway.h"
#include "barkway-enums.h"
#include <mildenhall/mildenhall.h>
#include "beckfoot.h"
#include "connection_manager.h"

G_BEGIN_DECLS

#define mildenhall_STATUS_BAR_DEFAULT_APP_ICON     ""
#define mildenhall_STATUS_BAR_DEFAULT_TEXT	"Apertis"
#define MAX_APPLICATIONS	6
#define MILLI_SECOND 1000

#define WIFI_SETTINGS_SIGNAL_STRENGTH(i) g_strconcat("/icon_SignalStrength_",g_strdup_printf ("%d",i),".png", NULL)

#define STATUS_TYPE_BAR status_bar_get_type()

#define STATUS_BAR(obj) \
		(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
				STATUS_TYPE_BAR, StatusBar))

#define STATUS_BAR_CLASS(klass) \
		(G_TYPE_CHECK_CLASS_CAST ((klass), \
				STATUS_TYPE_BAR, StatusBarClass))

#define STATUS_IS_BAR(obj) \
		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
				STATUS_TYPE_BAR))

#define STATUS_IS_BAR_CLASS(klass) \
		(G_TYPE_CHECK_CLASS_TYPE ((klass), \
				STATUS_TYPE_BAR))

#define STATUS_BAR_GET_CLASS(obj) \
		(G_TYPE_INSTANCE_GET_CLASS ((obj), \
				STATUS_TYPE_BAR, StatusBarClass))

typedef struct _StatusBar StatusBar;
typedef struct _StatusBarClass StatusBarClass;
typedef struct _StatusBarPrivate StatusBarPrivate;

struct _StatusBar
{
	ClutterActor parent;

	StatusBarPrivate *priv;
};

struct _StatusBarClass
{
	ClutterActorClass parent_class;
};

struct _StatusBarPrivate
{
	int expandedTexture;
	int onClicked;

	ClutterActor    *statusBarTime;       	// Status bar time
	ClutterActor 	*statusBarBg;			//Background of the stats bar
	ClutterActor	*statusIconPath;		//Status Bar Icon
	ClutterActor 	*statusBarContainer;
	ClutterActor	*darkImage;

	ClutterActor	*networkIcon;
	ClutterActor	*umtsIcon;

	ClutterActor	*textureOne;
	ClutterActor	*textureTwo;
	ClutterActor	*textureThree;
	ClutterActor	*textureFour;
	ClutterActor	*textureFive;
	ClutterActor	*textureSix;

	ClutterActor	*dividerLineOne;
	ClutterActor	*dividerLineTwo;
	ClutterActor	*dividerLineThree;
	ClutterActor	*dividerLineFour;
	ClutterActor	*dividerLineFive;
	ClutterActor	*dividerLineSix;

	ClutterActor	*statusInfo1;			//Status Bar information
	ClutterActor	*statusInfo2;
	ClutterActor	*statusInfo3;
	ClutterActor	*statusInfo4;
	ClutterActor	*statusInfo5;
	ClutterActor	*statusInfo6;

	ClutterActor	*leftArrowTexture;
	ClutterActor	*rightArrowTexture;

	ClutterActor	*leftDividerLine;
	ClutterActor	*rightDividerLine;

	GHashTable		*pStyleHash;
	GHashTable		*pLocalHash;

	SeatonPreference *sqliteObj;

	GList *textureList;
	GList *dividerLineList;
	GList *statusInfoList;
	GList *appList;
	GList *iconList;
	GList *textList;
	CanterburyHardKeys* app_mgr_hk_proxy;
	BarkwayService *popup_proxy;
	BeckfootStatus *Conproxy;
	CanterburyAppDbHandler *pAppLaunchMgrProxy;
	CanterburyAppManager *app_mgr_proxy;
	gint inAppsCount;
	const gchar *curAudioSource;
	guint inAudioResourceType;
	
	gchar *pIconData;
	gchar *pStatusData;
	
	gchar *pInterruptStatusData;
	gchar *pInterruptIconData;
	gchar *arg_app_name;
	gchar *arg_sub_app_name;
	gchar *arg_icon_path;
	gchar *arg_status_bar_text;
	gchar *mildenhall_STATUS_BAR_APP_NAME;
	gchar *PKG_DATA_DIRECTORY;

	CbyEntryPointIndex *entry_point_index;
};

GType status_bar_get_type (void) G_GNUC_CONST;
ClutterActor *mildenhall_status_bar_create(gchar *pAppName);
StatusBar *status_bar_new (void);

void update_status_bar(GObject *object,gchar *name);
void hide_or_show_actors(gpointer userData);
void status_bar_free_pkgdatadir_text(gchar *msg_text);
//#define mildenhall_STATUS_BAR_APP_NAME "Status-Bar"

#define ERROR(format, ...) g_error ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define MESSAGE(format, ...) g_message ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define INFO(format, ...) g_info ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

#define status_bar_debug(format, ...) DEBUG (format, ##__VA_ARGS__)

G_END_DECLS

#endif /* __STATUS_BAR_H__ */
